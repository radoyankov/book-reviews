package bg.devlabs.bookreviews.ui.home

import android.os.Build
import android.support.v4.content.ContextCompat
import android.text.Html
import android.text.method.LinkMovementMethod
import android.view.View
import android.view.animation.OvershootInterpolator
import android.webkit.JavascriptInterface
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.LinearLayout
import bg.devlabs.bookreviews.R
import bg.devlabs.bookreviews.data.api.GoodreadsAPI
import bg.devlabs.bookreviews.data.models.FullBook
import bg.devlabs.bookreviews.data.models.LocalBook
import bg.devlabs.bookreviews.utils.*
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.layout_book.*
import kotlinx.coroutines.experimental.async

/**
 * Created by Radoslav Yankov on 31.10.2017
 * Dev Labs
 * radoslavyankov@gmail.com
 */
class BookDelegate(val activity: HomeActivity, val goodreadsApi: GoodreadsAPI, override val containerView: View?, private val presenter: HomePresenter<HomeContract.View>?) : LayoutContainer {
    private lateinit var currentBook: FullBook
    var reviewsPage = 1
    var currentIsbn = ""
//    private var goodreadsApi = GoodreadsAPI(activity, ApiEventListener())
    /**
     * Fills views with information from book
     * @param book object with info on book
     */
    fun fillViews(book: FullBook) {
        async {
            presenter?.insert(LocalBook(book.id, book.title, book.image, book.description, book.rating,
                    book.author, book.ratings_widget, book.link))
        }
        currentIsbn = book.id
        currentBook = book
        startImageAnimation()
        image.load(book.image)
        name.text = book.title
        author.text = book.author
        description.text = Html.fromHtml(book.description)
        description.movementMethod = LinkMovementMethod.getInstance()
        star_text.text = book.rating
        next_page.visibility = View.INVISIBLE
        enablePageButton()

        save.setOnTouchListener(AnimatedOnTouch)
        next_page.setOnTouchListener(AnimatedOnTouch)

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            save.setTextColor(ContextCompat.getColor(save.context, R.color.text_color))
        }
        //works
//        requestIfLoggedIn {
//            goodreadsApi.getHome()
//        }

        save.onClick {
            saveBook(book)
        }

        close.onClick {
            presenter?.hideBookDrawer()
        }

        link.onClick {
            containerView?.context?.openWeb(book.link)
        }

        delay(mainLayoutAniDelay) {
            //container slide in animation
            presenter?.showBookDrawer()

            //yellow stars animation
            arrayListOf(star1, star2, star3, star4, star5).forEachIndexed { index, image ->
                image.apply {
                    setImageResource(R.drawable.ic_star_border_black_24dp)
                    setTint(R.color.colorBlack)
                    val tempResId: Int = if (book.rating.toDouble() >= index + 1 - 0.2) {
                        R.drawable.ic_star_black_24dp
                    } else {
                        if (book.rating.toDouble() < index + 2 && index + 0.5 <= book.rating.toDouble()) {
                            R.drawable.ic_star_half_black_24dp
                        } else {
                            return@forEachIndexed
                        }
                    }

                    delay(starAniDelay + 300 + index * 70) {
                        scaleX = 0f
                        scaleY = 0f
                        setImageResource(tempResId)
                        setTint(R.color.colorStars)
                        animation {
                            scaleX(1f)
                            scaleY(1f)
                            duration = 1200
                            interpolator = OvershootInterpolator()
                        }
                    }
                }
            }
        }
    }

    /**
     * Adds a webview with the next review page to the reviews linear layout
     */
    private fun addPage() {
        next_page.onClick(null)

        next_page.rotateAndChange {
            next_page.text = containerView?.context?.getString(R.string.loading)
        }

        disablePageButton()
        presenter?.getBook(currentIsbn, reviewsPage)
    }

    /**
     * Enables the "More Reviews" button
     */
    fun enablePageButton() {
        next_page.text = containerView?.context?.getString(R.string.more_reviews)
        next_page.onClick {
            if (reviewsPage < 20) {
                reviewsPage++
                addPage()
            }
        }
    }

    /**
     * Disables the "More Reviews" button
     */
    private fun disablePageButton() = next_page.onClick(null)

    /**
     * Resets the book view when the drawer is closed
     */
    fun resetBook() {
        image_wrapper.alpha = 0f
        layout_web_array.removeAllViews()
    }

    /**
     * Starts the book cover animation
     */
    private fun startImageAnimation() {
        delay(bookCoverAniDelay) {
            image_wrapper.apply {
                alpha = 0f
                scaleX = 1.4f
                scaleY = 1.4f
                rotationX = 10f
                animation {
                    duration = bookCoverAniDuration
                    alpha(1f)
                    scaleX(1f)
                    scaleY(1f)
                    rotationX(0f)
                }
            }
        }
    }

    /**
     * Sets up a webview for the reviews of a book
     * The webView is needed because Goodreads doesn't provide any other API alternative
     */
    fun setupWebView(book: FullBook) {
        val webReviews = WebView(containerView?.context)
        val layout = containerView?.findViewById<LinearLayout>(R.id.layout_web_array) as LinearLayout
        layout.addView(webReviews, layout.childCount)
        webReviews.isVerticalScrollBarEnabled = false
        webReviews.visibility = View.GONE

        webReviews.apply {
            var isViewClean = false
            val classToIgnore = arrayListOf("gr_reviews_showing", "gr_pagination")

            settings.loadsImagesAutomatically = false
            settings.useWideViewPort = true
            settings.domStorageEnabled = true

            open class JsInterface
            addJavascriptInterface(object : JsInterface() {
                @JavascriptInterface
                fun processHTML(html: String) {
                    //formatting HTML
                    val formattedHtml = html
                            .replace("★", "<font color=\"#fccf1a\">★</font>")
                    val htmlBody = "<link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\" />$formattedHtml"
                    containerView.post {
                        loadDataWithBaseURL("file:///android_asset/", htmlBody, "text/html", "utf-8",
                                null)
                    }
                }
            }, "HTMLOUT")

            settings.javaScriptEnabled = true
            webViewClient = object : WebViewClient() {
                override fun onPageFinished(view: WebView, url: String) {
                    if (!isViewClean) {
                        //cleans the "write a review" button
                        for (each in classToIgnore) {
                            loadUrl("javascript:if (typeof(document.getElementsByClassName('$each')[0]) != 'undefined' && document.getElementsByClassName('$each')[0] != null){document.getElementsByClassName('$each')[0].style.display = 'none';} void 0")
                        }
                        loadUrl("javascript:window.HTMLOUT.processHTML('<head>'+document.getElementsByTagName('html')[0].innerHTML+'</head>');")

                        isViewClean = true

                    } else {
                        visibility = View.VISIBLE
                        enablePageButton()
                        next_page.visibility = View.VISIBLE
                    }
                }

                override fun shouldOverrideUrlLoading(view: WebView, url: String?): Boolean {
                    return if (url != null && (url.startsWith("http://") || url.startsWith("https://"))) {
                        containerView.context.openWeb(url, context)
                        true
                    } else {
                        false
                    }
                }
            }
            loadUrl(book.ratings_widget)
        }
    }

    private fun saveBook(book: FullBook) {
        requestIfLoggedIn {
            if (goodreadsApi.addToShelf(book.id, false, "to-read")?.id != null) {
                activity.runOnUiThread {
                    save.rotateAndChange {
                        save.onClick { removeBook(book) }
                        save.text = save.context.getString(R.string.remove)
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            save.setTextColor(save.context.getColor(R.color.text_color))
                            save.backgroundTintList = null
                        }
                    }
                }
            }
        }
    }

    private fun removeBook(book: FullBook) {
        requestIfLoggedIn {
            activity.runOnUiThread {
                save.rotateAndChange {
                    save.onClick { saveBook(book) }
                    save.text = save.context.getString(R.string.want_to_read)
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        save.setTextColor(save.context.getColor(R.color.white))
                        save.backgroundTintList = save.context.getColorStateList(R.color.green)
                    }
                }
            }

        }
    }

    fun requestIfLoggedIn(func: () -> Unit) {
        goodreadsApi.requestIfLoggedIn(func)
    }

}