package bg.devlabs.bookreviews.ui.home

import bg.devlabs.bookreviews.data.DataManager
import bg.devlabs.bookreviews.data.local.LocalBookDao
import bg.devlabs.bookreviews.data.models.FullBook
import bg.devlabs.bookreviews.data.models.LocalBook
import bg.devlabs.bookreviews.ui.base.BasePresenter
import bg.devlabs.bookreviews.utils.Error
import io.reactivex.disposables.CompositeDisposable
import java.io.FileNotFoundException
import javax.inject.Inject

/**
 * Created by Radoslav Yankov on 26.10.2017
 * Dev Labs
 * radoslavyankov@gmail.com
 */
class HomePresenter<V : HomeContract.View> @Inject
constructor(dataManager: DataManager, compositeDisposable: CompositeDisposable, val localBooks: LocalBookDao) : BasePresenter<V>(dataManager, compositeDisposable), HomeContract.Presenter<V> {
    private var currentBook: FullBook? = null

    override fun getBook(isbn: String, page: Int) {
        dataManager.getBook(isbn, page).subscribe({ response ->
            response.ratings_widget = dataManager.getBookReviewsLink(isbn, page)
            if (page <= 1) {
                view.setupViews(response)
                dataManager.lastBook = response
            } else {
                view.setupWebView(response)
            }
        }, { error ->
            view.startCamera()
            if (error is FileNotFoundException) {
                view.showToast(Error.BOOK_NOT_FOUND)
            } else {
            }
        })

    }

    override fun saveUserId(userId: String) {
        dataManager.userId = userId
    }

    override fun handleLogin() {
        view.handleLogin()
    }

    override fun checkLastBook() {
        dataManager.lastBook.apply {
            if (this != null) {
                view.showPreviousButton()
                currentBook = this
            }
        }
    }

    override fun loadLastBook() {
        currentBook?.let {
            view.setupViews(it)
        }
    }

    override fun finishTutorial() {
        dataManager.onboardingPassed = true
        view.finishTutorial()
    }

    override fun checkTutorial() {
        if (dataManager.onboardingPassed) {
            view.skipTutorial()
        } else {
            view.showTutorial()
        }
    }

    override fun showBookDrawer() = view.showBookDrawer()

    override fun hideBookDrawer() = view.hideBookDrawer()

    override fun getLocals() = localBooks.getLocals()

    override fun getLocalsById(id: String) = localBooks.getLocalsById(id)

    override fun deleteAll() = localBooks.deleteAll()

    override fun insert(book: LocalBook) = localBooks.insert(book)

    override fun delete(book: LocalBook) = localBooks.delete(book)
}
