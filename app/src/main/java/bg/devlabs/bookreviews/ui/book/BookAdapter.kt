package bg.devlabs.bookreviews.ui.book

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import bg.devlabs.bookreviews.R
import bg.devlabs.bookreviews.data.api.xml.responses.ShelfResponse
import bg.devlabs.bookreviews.ui.bookshelves.BookshelvesContract
import bg.devlabs.bookreviews.utils.inflate

/**
 * Created by Radoslav Yankov on 03.05.2018
 * Dev Labs
 * Adapter class for a book list
 * radoslavyankov@gmail.com
 */
class BookAdapter(val presenter: BookshelvesContract.Presenter<BookshelvesContract.View>, val data: ShelfResponse) : RecyclerView.Adapter<BookViewHolder>() {
//    val

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int) = BookViewHolder(presenter, parent?.inflate(R.layout.item_book))

    override fun getItemCount() = data.reviews?.size?:0

    override fun onBindViewHolder(holder: BookViewHolder, position: Int) {
        holder.bind(data.reviews?.get(position)?.book)
    }

}