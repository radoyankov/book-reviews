package bg.devlabs.bookreviews.ui.feed

import android.support.v7.widget.RecyclerView
import android.view.View
import bg.devlabs.bookreviews.data.api.GoodreadsAPI
import bg.devlabs.bookreviews.data.api.xml.objects.Update
import kotlinx.android.extensions.LayoutContainer

abstract class BaseViewHolder(view: View) : RecyclerView.ViewHolder(view), LayoutContainer {
    open fun bind(entry: Update?, goodreadsApi: GoodreadsAPI) {

    }
}
