package bg.devlabs.bookreviews.ui.bookshelves

import bg.devlabs.bookreviews.data.api.xml.responses.UserResponse
import bg.devlabs.bookreviews.ui.base.BaseContract

class BookshelvesContract {
    interface View: BaseContract.View {

        fun onDataFetched(data: UserResponse)

        fun onBookClicked(isbn: String)
    }

    interface Presenter<in V : BookshelvesContract.View> : BaseContract.Presenter<V>{
        fun getUserId(): String?
        fun fetchBook(isbn: String)
    }

}
