package bg.devlabs.bookreviews.ui.home

import android.view.View
import bg.devlabs.bookreviews.R
import bg.devlabs.bookreviews.utils.*
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.layout_onboarding.*

/**
 * Created by Radoslav Yankov on 31.10.2017
 * Dev Labs
 * radoslavyankov@gmail.com
 */
open class TutorialDelegate(override val containerView: View?, private val presenter: HomePresenter<HomeContract.View>?) : LayoutContainer {
    init {
        arrayListOf(onboarding_view, static_description).forEach {
            it.translationY = 50f
            it.alpha = 0f
            done.rotationX = 90f
            delay(500) {
                it.animation {
                    translationY(0f)
                    alpha(1f)
                    duration = 400
                }
                done.animation {
                    rotationX(0f)
                    duration = 400
                }
            }
        }

        done.setOnTouchListener(AnimatedOnTouch)
        delay(500) {
            onboarding_view.load(R.raw.onboarding)
        }

        done.onClick {
            delay(400) {
                val oldY = navigation.y
                navigation.apply {
                    y += 100
                    visibility = View.VISIBLE
                    animate().y(oldY)
                }
            }
            onboarding_view.visibility = View.GONE
            presenter?.finishTutorial()
        }
    }

}