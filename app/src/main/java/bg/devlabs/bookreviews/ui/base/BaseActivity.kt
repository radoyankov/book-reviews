package bg.devlabs.bookreviews.ui.base

import android.content.Context
import android.content.DialogInterface
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.inputmethod.InputMethodManager
import android.widget.Toast

import bg.devlabs.bookreviews.R
import bg.devlabs.bookreviews.utils.Error
import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Point
import android.os.Bundle
import android.os.Handler
import android.os.Message
import com.afollestad.materialdialogs.MaterialDialog
import com.tbruyelle.rxpermissions2.RxPermissions
import android.support.design.widget.Snackbar




/**
 * Created by Radoslav Yankov on 26.10.2017
 * Dev Labs
 * radoslavyankov@gmail.com
 */
@SuppressLint("Registered")
open class BaseActivity : AppCompatActivity(), BaseContract.View {
    var progressDialog: MaterialDialog? = null

    override fun showToast(messageResId: Error) {
        when (messageResId) {
            Error.BOOK_NOT_FOUND -> {
                showToast(getString(R.string.error_book_not_found))
            }
            Error.OTHER_ERROR -> {
                showToast(getString(R.string.error_generic))
            }
        }
    }

    override fun hideKeyboard() {
        (getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(this.currentFocus?.windowToken, 0)
    }

    override fun shareText(text: String) {
        val intent = Intent(Intent.ACTION_SEND)
        intent.type = "text/plain"
        intent.putExtra(Intent.EXTRA_TEXT, text)
        startActivity(intent)
    }

    override fun openActivity(activityClass: Class<*>, names: Array<String>, values: Array<String>) {
        val intent = Intent(this, activityClass)
        val size = names.size
        val size2 = values.size
        if (size != size2) {
//            Log.d(TAG, "openActivity: size!=size2")
            return
        }
        for (i in 0 until size) {
            intent.putExtra(names[i], values[i])
        }
        startActivity(intent)
    }

    private var toast: Toast? = null

    var snackbar: Snackbar? = null

    override fun showErrorDialog(message: String) {
        AlertDialog.Builder(this)
                .setTitle(R.string.dialog_error)
                .setMessage(message)
                .setPositiveButton(getString(R.string.dialog_ok), EmptyOnClickListener())
                .show()
    }

    override fun showErrorDialog(messageResId: Int) {
        AlertDialog.Builder(this)
                .setTitle(R.string.dialog_error)
                .setMessage(messageResId)
                .setPositiveButton(R.string.dialog_ok, EmptyOnClickListener())
                .show()
    }

    override fun showToast(message: String) {
        if (toast != null) {
            toast?.cancel()
        }

        toast = Toast.makeText(applicationContext, message, Toast.LENGTH_SHORT)
        toast?.show()
    }

    override fun showToast(messageResId: Int) = showToast(getString(messageResId))


    override fun getScreenSize(): Pair<Int, Int> {
        val display = windowManager.defaultDisplay
        val size = Point()
        display.getSize(size)
        return Pair(size.x, size.y)
    }

    override fun requestPermission(permission: String, callback: Handler.Callback) {
        RxPermissions(this).request(permission)
                .subscribe { isGranted ->
                    val bundle = Bundle(1)
                    val message = Message()
                    bundle.putBoolean("permission", isGranted)
                    message.data = bundle
                    callback.handleMessage(message)
                }
    }

    private class EmptyOnClickListener : DialogInterface.OnClickListener {
        override fun onClick(dialogInterface: DialogInterface, i: Int) {

        }
    }

    override fun showProgressDialog() {
        if (isFinishing) {
            return
        }

        progressDialog?.let {
            if (!it.isShowing) {
                showProgressSimple()
            }
        } ?: run {
            showProgressSimple()
        }
    }

    private fun showProgressSimple() {
        progressDialog = MaterialDialog.Builder(this)
                .title(getString(R.string.updating))
                .content(getString(R.string.please_wait))
                .progress(true, 0)
                .show()
    }

    override fun dismissProgressDialog() {
        if (isFinishing) {
            return
        }
        progressDialog?.let {
            if (it.isShowing) {
                it.dismiss()
            }
        }
    }
}
