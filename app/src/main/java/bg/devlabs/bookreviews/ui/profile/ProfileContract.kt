package bg.devlabs.bookreviews.ui.bookshelves

import bg.devlabs.bookreviews.data.api.xml.responses.UserResponse
import bg.devlabs.bookreviews.data.models.LocalBook
import bg.devlabs.bookreviews.ui.base.BaseContract
import io.reactivex.Single

class ProfileContract {
    interface View: BaseContract.View {

        fun onDataFetched(data: UserResponse)
        fun onBookClicked(book: LocalBook)
    }

    interface Presenter<in V : ProfileContract.View> : BaseContract.Presenter<V>{
        /**
         * Returns the current user ID
         */
        fun getUserId(): String?

        /**
         * Returns a list of locally saved books
         */
        fun getLocals(): Single<List<LocalBook>>

        /**
         * Returns a single locally saved book
         */
        fun getLocalsById(id: String): Single<LocalBook>

        /**
         * Deletes all locally saved books
         */
        fun deleteAll()

        /**
         * Deletes one locally saved book
         */
        fun deleteById(id: String)

        /**
         * Gets one locally saved book
         */
        fun fetchBook(isbn: String)
    }

}
