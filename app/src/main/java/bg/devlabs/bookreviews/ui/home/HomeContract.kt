package bg.devlabs.bookreviews.ui.home

import bg.devlabs.bookreviews.data.models.FullBook
import bg.devlabs.bookreviews.data.models.LocalBook
import bg.devlabs.bookreviews.ui.base.BaseContract
import io.reactivex.Single

/**
 * Created by Radoslav Yankov on 26.10.2017
 * Dev Labs
 * radoslavyankov@gmail.com
 */

interface HomeContract {
    interface View : BaseContract.View {
        /**
         * Initial setup of views
         */
        fun setupViews(book: FullBook)

        /**
         * Starts the camera for barcode scanning
         */
        fun startCamera()

        /**
         * Sets up only the webview part of the book view
         */
        fun setupWebView(book: FullBook)

        /**
         * shows the "previous book" button
         */
        fun showPreviousButton()

        /**
         * shows the app tutorial
         */
        fun showTutorial()

        /**
         * skips the app tutorial and proceeds to the camera
         */
        fun skipTutorial()

        /**
         * finishes the tutorial
         */
        fun finishTutorial()

        fun showBookDrawer()

        fun hideBookDrawer()

        fun handleLogin()

        fun showBook(isbn: String)
    }

    interface Presenter<in V : HomeContract.View> : BaseContract.Presenter<V> {
        /**
         * Gathers book data from data manager endpoint
         */
        fun getBook(isbn: String, page: Int)

        /**
         * Check if the user has finished the onboarding
         */
        fun checkTutorial()

        /**
         * Finishes the tutorial and saves the state in the database
         */
        fun finishTutorial()

        /**
         * Loads and passes the last views book to the view
         */
        fun loadLastBook()

        /**
         * Checks if a last views book is available
         */
        fun checkLastBook()
        fun showBookDrawer()
        fun hideBookDrawer()
        fun handleLogin()

        /**
         * Saves the user ID
         */
        fun saveUserId(userId: String)
        //gets a list of locally saved books
        fun getLocals(): Single<List<LocalBook>>
        //gets a single locally saved book
        fun getLocalsById(id: String): Single<LocalBook>
        fun deleteAll()

        //inserts a book in the DB
        fun insert(book: LocalBook)
        //deletes a book form the DB
        fun delete(book: LocalBook): Int
    }
}
