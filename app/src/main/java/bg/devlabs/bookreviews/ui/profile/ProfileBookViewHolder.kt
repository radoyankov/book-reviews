package bg.devlabs.bookreviews.ui.book

import android.support.v7.widget.RecyclerView
import android.view.View
import bg.devlabs.bookreviews.data.models.LocalBook
import bg.devlabs.bookreviews.ui.bookshelves.ProfileContract
import bg.devlabs.bookreviews.utils.load
import bg.devlabs.bookreviews.utils.onClick
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_book.*

/**
 * Created by Radoslav Yankov on 03.05.2018
 * Dev Labs
 * Class for displaying a book in the profile screen book list
 * radoslavyankov@gmail.com
 */
class ProfileBookViewHolder(val presenter: ProfileContract.Presenter<ProfileContract.View>, override val containerView: View?) : RecyclerView.ViewHolder(containerView), LayoutContainer {
    fun bind(book: LocalBook?) {
        book_image.load(book?.image)
        title_text.text = book?.title.orEmpty()
        container.onClick {
            book?.id?.let {
                presenter.fetchBook(it)
                presenter.showProgressDialog()
            }
        }
    }
}