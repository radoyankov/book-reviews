package bg.devlabs.bookreviews.ui.bookshelves

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import bg.devlabs.bookreviews.R
import bg.devlabs.bookreviews.data.api.GoodreadsAPI
import bg.devlabs.bookreviews.data.api.xml.responses.Work

/**
 * Created by Radoslav Yankov on 03.05.2018
 * Dev Labs
 * Adapter class for the book list in the search section
 * radoslavyankov@gmail.com
 */

class BookshelvesSearchAdapter(val userId: String, val presenter: BookshelvesPresenter<BookshelvesContract.View>, val data: List<Work>?, val goodreadsApi: GoodreadsAPI) : RecyclerView.Adapter<BookshelfSearchViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BookshelfSearchViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val containerView = inflater.inflate(R.layout.item_search_result, parent, false)
        return BookshelfSearchViewHolder(presenter, containerView)
    }
    override fun getItemCount() = data?.size?:0

    override fun onBindViewHolder(holder: BookshelfSearchViewHolder, position: Int) {
        holder.bind(userId, data?.get(position), goodreadsApi)
        if (position == (data?.size?:0) - 1){
            presenter.dismissProgressDialog()
        }
    }

}