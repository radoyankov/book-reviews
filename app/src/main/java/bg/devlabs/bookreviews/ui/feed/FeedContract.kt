package bg.devlabs.bookreviews.ui.bookshelves

import bg.devlabs.bookreviews.data.api.xml.responses.FeedResponse
import bg.devlabs.bookreviews.ui.base.BaseContract

class FeedContract {
    interface View: BaseContract.View {

        fun onDataFetched(data: FeedResponse)

        fun onBookClicked(isbn: String)

        fun onUserClicked(id: String)
    }

    interface Presenter<in V : FeedContract.View> : BaseContract.Presenter<V>{
        //handles a click on a book cover
        fun fetchBook(isbn: String)

        //handles a click on a user image
        fun fetchUser(id: String)
    }

}
