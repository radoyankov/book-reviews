package bg.devlabs.bookreviews.ui.bookshelves

import android.text.Html
import android.view.View
import bg.devlabs.bookreviews.data.api.GoodreadsAPI
import bg.devlabs.bookreviews.data.api.xml.objects.ReadStatus
import bg.devlabs.bookreviews.data.api.xml.objects.Update
import bg.devlabs.bookreviews.ui.feed.BaseViewHolder
import bg.devlabs.bookreviews.utils.load
import bg.devlabs.bookreviews.utils.loadCircle
import bg.devlabs.bookreviews.utils.onClick
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_update.*

/**
 * Created by Radoslav Yankov on 03.05.2018
 * Dev Labs
 * radoslavyankov@gmail.com
 */
class FeedReadViewHolder(val presenter: FeedContract.Presenter<FeedContract.View>,
                         override val containerView: View) : BaseViewHolder(containerView), LayoutContainer {

    override fun bind(entry: Update?, goodreadsApi: GoodreadsAPI) {
        (entry?.obj?.obj as ReadStatus).review?.book?.imageUrl
        book_image.load("https://s.gr-assets.com/assets/nophoto/book/50x75-a91bf249278a81aabab721ef782c4a74.png")
        book_image.onClick { presenter.fetchBook(entry.obj.obj.review?.book?.isbn13 ?: "") }
        book_title_text.text = (entry.obj.obj.review?.book?.title)
        book_author_text.text = "by " + (entry.obj.obj.review?.book?.author?.name)
        status_text.text = entry.actor?.name + " " + Html.fromHtml(entry.action_text)
        person_image.loadCircle((entry.image_url))
        person_image.onClick { presenter.fetchUser(entry.actor?.id.toString()) }
        book_extra_text.text = Html.fromHtml("<i>" + (entry.obj.obj.review?.book?.description
                ?: "") + "</i>")
        book_extra_text.textSize = 12f
    }
}