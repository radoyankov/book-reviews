package bg.devlabs.bookreviews.ui.bookshelves

import android.content.Context
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.View.*
import android.view.ViewGroup
import android.widget.EditText
import bg.devlabs.bookreviews.R
import bg.devlabs.bookreviews.data.api.GoodreadsAPI
import bg.devlabs.bookreviews.data.api.xml.responses.UserResponse
import bg.devlabs.bookreviews.ui.base.BaseFragment
import bg.devlabs.bookreviews.ui.home.HomeActivity
import bg.devlabs.bookreviews.utils.delay
import bg.devlabs.bookreviews.utils.onClick
import com.afollestad.materialdialogs.MaterialDialog
import com.jakewharton.rxbinding2.widget.RxTextView
import dagger.android.support.AndroidSupportInjection
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_bookshelves.*
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import java.util.concurrent.TimeUnit
import javax.inject.Inject

/**
 * Created by Radoslav Yankov on 03.05.2018
 * Dev Labs
 * radoslavyankov@gmail.com
 */
class BookshelvesFragment : BaseFragment(), BookshelvesContract.View {
    @Inject
    lateinit var presenter: BookshelvesPresenter<BookshelvesContract.View>
    lateinit var adapter: BookshelvesAdapter
    lateinit var goodreadsApi: GoodreadsAPI
    var userId = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {


        return inflater.inflate(R.layout.fragment_bookshelves, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        container.visibility = GONE
        presenter.showProgressDialog()
        goodreadsApi.requestIfLoggedIn {
            async(UI) {
                val data = async {
                    goodreadsApi.getUser(if (userId.isEmpty()) presenter.getUserId()
                            ?: "" else userId)
                }.await()
                onDataFetched(data)
            }
        }
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onDataFetched(data: UserResponse) {
        adapter = BookshelvesAdapter(userId, presenter, data.user.user_shelves, goodreadsApi)
        list.adapter = adapter
        list.layoutManager = LinearLayoutManager(context)
        header_text.text = "Bookshelves"
        container.visibility = VISIBLE
        container.alpha = 0f
        container.animate().alpha(1f)
        delay(500) {
            container.requestFocus()
            search_text.clearFocus()
            hideKeyboard()
        }
        search_results_card.visibility = GONE

        if (tag == getString(R.string.fragment_bookshelves_inner)) {
            add_shelf_layout.visibility = GONE
            search_text.visibility = GONE
        } else {
            add_shelf_layout.onClick {
                showAddShelfDialog()
            }
            setupSearch()
        }
    }

    private fun setupSearch() {
        RxTextView.textChanges(search_text)
                .debounce(1000, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    if (it.toString().isEmpty()) {
                        search_results_card.visibility = GONE
                    } else {
                        search_progress.visibility = VISIBLE
                        async(UI) {
                            val data = async {
                                goodreadsApi.search(it.toString())
                            }.await()
                            if (data.search.results?.size ?: 0 > 0) {
                                search_results_card.visibility = VISIBLE
                                search_results.layoutManager = LinearLayoutManager(getApplicationContext())
                                search_results.adapter = BookshelvesSearchAdapter(userId, presenter, data.search.results, goodreadsApi)
                            }
                            search_progress.visibility = GONE
                        }
                    }
                }
    }

    private fun showAddShelfDialog() {
        context?.let {
            MaterialDialog.Builder(it)
                .customView(R.layout.dialog_bookshelf, true)
                .positiveText(getString(R.string.add_shelf))
                .onPositive { dialog, _ ->
                    async(UI) {
                        try {

                            val data = async {
                                val text = dialog.customView?.findViewById<EditText>(R.id.shelf_text)?.text?.toString()
                                goodreadsApi.createShelf(text ?: "shelf")
                            }.await()
                            showToast(getString(R.string.add_shelf_success))
                        } catch (e: Exception) {
                            showToast(getString(R.string.add_shelf_error))
                        }
                    }
                }
                .show()
        }
    }

    override fun onBookClicked(isbn: String) {
        (activity as HomeActivity).showBook(isbn)
    }

    override fun onAttach(context: Context?) {
        AndroidSupportInjection.inject(this)
        presenter.onAttach(this)
        super.onAttach(context)
    }

    override fun onDetach() {
        super.onDetach()
        presenter.onDetach()
    }

}