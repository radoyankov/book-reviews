package bg.devlabs.bookreviews.ui.base

import android.content.Context
import android.os.Handler
import bg.devlabs.bookreviews.utils.Error

/**
 * Created by Radoslav Yankov on 26.10.2017
 * Dev Labs
 * radoslavyankov@gmail.com
 */
class BaseContract{

    interface View {
        fun showErrorDialog(message: String)

        fun showErrorDialog(messageResId: Int)

        fun showProgressDialog()

        fun dismissProgressDialog()

        fun showToast(message: String)

        fun showToast(messageResId: Int)

        fun showToast(messageResId: Error)

        fun getApplicationContext(): Context

        fun getString(stringResId: Int): String

        fun hideKeyboard()

        fun getScreenSize(): Pair<Int, Int>

        fun requestPermission(permission: String, callback: Handler.Callback)

        fun openActivity(activityClass: Class<*>, names: Array<String>, values: Array<String>)

        fun shareText(text: String)

    }

    interface Presenter<in V : BaseContract.View> {
        fun onAttach(view: V)

        fun onDetach()

        fun showProgressDialog()

        fun dismissProgressDialog()

    }
    }