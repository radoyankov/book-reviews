package bg.devlabs.bookreviews.ui.bookshelves

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import bg.devlabs.bookreviews.R
import bg.devlabs.bookreviews.data.api.GoodreadsAPI
import bg.devlabs.bookreviews.data.api.xml.objects.ReadStatus
import bg.devlabs.bookreviews.data.api.xml.responses.FeedResponse
import bg.devlabs.bookreviews.ui.feed.BaseViewHolder

class FeedAdapter(val presenter: FeedPresenter<FeedContract.View>, val data: FeedResponse?, val goodreadsApi: GoodreadsAPI) : RecyclerView.Adapter<BaseViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return when (viewType) {
            READ_STATUS -> {
                val containerView = inflater.inflate(R.layout.item_update, parent, false)
                FeedReadViewHolder(presenter, containerView)
            }
        //BOOK_STATUS
            else -> {
                val containerView = inflater.inflate(R.layout.item_update, parent, false)
                FeedBookViewHolder(presenter, containerView)
            }
        }
    }

    override fun getItemCount() = data?.updates?.size ?: 0

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        holder.bind(data?.updates?.get(position), goodreadsApi)


    }

    override fun getItemViewType(position: Int): Int {
        return data?.updates?.get(position)?.let {
            if (it.obj?.obj is ReadStatus) {
                READ_STATUS
            } else {
                BOOK_STATUS
            }
        } ?: BOOK_STATUS
    }

}

const val READ_STATUS = 1
const val BOOK_STATUS = 2