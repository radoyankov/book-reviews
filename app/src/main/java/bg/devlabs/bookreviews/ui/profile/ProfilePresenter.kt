package bg.devlabs.bookreviews.ui.bookshelves

import bg.devlabs.bookreviews.data.DataManager
import bg.devlabs.bookreviews.data.api.xml.responses.ShelfResponse
import bg.devlabs.bookreviews.data.local.LocalBookDao
import bg.devlabs.bookreviews.ui.base.BasePresenter
import bg.devlabs.bookreviews.ui.base.BasePresenter_Factory
import bg.devlabs.bookreviews.ui.home.HomeContract
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
 * Created by Radoslav Yankov on 03.05.2018
 * Dev Labs
 * radoslavyankov@gmail.com
 */
class ProfilePresenter<V : ProfileContract.View> @Inject
constructor(dataManager: DataManager, compositeDisposable: CompositeDisposable, val localBooks: LocalBookDao) : BasePresenter<V>(dataManager, compositeDisposable), ProfileContract.Presenter<V> {
    override fun getUserId() = dataManager.userId

    override fun getLocals() = localBooks.getLocals()

    override fun getLocalsById(id: String) = localBooks.getLocalsById(id)

    override fun deleteAll() = localBooks.deleteAll()

    override fun deleteById(id: String) = localBooks.deleteById(id)

    override fun fetchBook(isbn: String) {
        localBooks.getLocalsById(isbn)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    view.onBookClicked(it)
                }, {
                })
    }
}