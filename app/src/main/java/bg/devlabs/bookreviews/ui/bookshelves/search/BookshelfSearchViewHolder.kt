package bg.devlabs.bookreviews.ui.bookshelves

import android.support.v7.widget.RecyclerView
import android.view.View
import bg.devlabs.bookreviews.data.api.GoodreadsAPI
import bg.devlabs.bookreviews.data.api.xml.responses.Work
import bg.devlabs.bookreviews.utils.load
import bg.devlabs.bookreviews.utils.onClick
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_search_result.*

/**
 * Created by Radoslav Yankov on 03.05.2018
 * Dev Labs
 * Class for displaying a book in the search section list
 * radoslavyankov@gmail.com
 */
class BookshelfSearchViewHolder(val presenter: BookshelvesContract.Presenter<BookshelvesContract.View>,
                          override val containerView: View?) : RecyclerView.ViewHolder(containerView), LayoutContainer {

    fun bind(userId: String, work: Work?, goodreadsApi: GoodreadsAPI) {
        image_search.load(work?.best_book?.imageUrl)
        title_text.text = work?.best_book?.title
        if (work?.best_book?.author?.name != null){
        extra_text.text = work.best_book.author?.name
        } else {
            extra_text.text = work?.best_book?.authors?.author?.name
        }
        container.onClick {
            presenter.fetchBook(work?.best_book?.id?:"")
        }
    }
}