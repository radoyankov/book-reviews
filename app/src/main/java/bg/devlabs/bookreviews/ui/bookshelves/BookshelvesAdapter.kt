package bg.devlabs.bookreviews.ui.bookshelves

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import bg.devlabs.bookreviews.R
import bg.devlabs.bookreviews.data.api.GoodreadsAPI
import bg.devlabs.bookreviews.data.api.xml.objects.UserShelf

/**
 * Created by Radoslav Yankov on 03.05.2018
 * Dev Labs
 * Adapter class for the bookshelf list screen
 * radoslavyankov@gmail.com
 */
class BookshelvesAdapter(val userId: String, val presenter: BookshelvesPresenter<BookshelvesContract.View>, val data: List<UserShelf>?,val goodreadsApi: GoodreadsAPI) : RecyclerView.Adapter<BookshelfViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BookshelfViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val containerView = inflater.inflate(R.layout.item_bookshelf, parent, false)
        return BookshelfViewHolder(presenter, containerView)
    }
    override fun getItemCount() = data?.size?:0

    override fun onBindViewHolder(holder: BookshelfViewHolder, position: Int) {
        holder.bind(userId, data?.get(position), goodreadsApi)
        if (position == (data?.size?:0) - 1){
            presenter.dismissProgressDialog()
        }
    }

}