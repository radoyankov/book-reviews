package bg.devlabs.bookreviews.ui.book

import android.support.v7.widget.RecyclerView
import android.view.View
import bg.devlabs.bookreviews.data.api.xml.objects.Book
import bg.devlabs.bookreviews.ui.bookshelves.BookshelvesContract
import bg.devlabs.bookreviews.utils.load
import bg.devlabs.bookreviews.utils.onClick
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_book.*

/**
 * Created by Radoslav Yankov on 03.05.2018
 * Dev Labs
 * Class for displaying a book item inside a book list
 * radoslavyankov@gmail.com
 */
class BookViewHolder(val presenter: BookshelvesContract.Presenter<BookshelvesContract.View>, override val containerView: View?) : RecyclerView.ViewHolder(containerView), LayoutContainer {
    fun bind(book: Book?) {
        book as Book
        book_image.load(book.imageUrl)
        title_text.text = book.title.orEmpty()
        container.onClick {
            book.isbn?.let {
                presenter.fetchBook(it)
                presenter.showProgressDialog()
            }
        }
    }
}