package bg.devlabs.bookreviews.ui.bookshelves

import bg.devlabs.bookreviews.data.DataManager
import bg.devlabs.bookreviews.ui.base.BasePresenter
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

/**
 * Created by Radoslav Yankov on 03.05.2018
 * Dev Labs
 * radoslavyankov@gmail.com
 */
class BookshelvesPresenter<V : BookshelvesContract.View>  @Inject
constructor(dataManager: DataManager, compositeDisposable: CompositeDisposable) : BasePresenter<V>(dataManager, compositeDisposable), BookshelvesContract.Presenter<V>{

    override fun getUserId() = dataManager.userId

    override fun fetchBook(isbn: String){
        view.onBookClicked(isbn)
    }
}