package bg.devlabs.bookreviews.ui.bookshelves

import android.content.Context
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import bg.devlabs.bookreviews.R
import bg.devlabs.bookreviews.data.api.GoodreadsAPI
import bg.devlabs.bookreviews.data.api.xml.responses.FeedResponse
import bg.devlabs.bookreviews.ui.base.BaseFragment
import bg.devlabs.bookreviews.ui.home.HomeActivity
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_list.*
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import javax.inject.Inject

/**
 * Created by Radoslav Yankov on 03.05.2018
 * Dev Labs
 * radoslavyankov@gmail.com
 */
class FeedFragment : BaseFragment(), FeedContract.View {

    @Inject
    lateinit var presenter: FeedPresenter<FeedContract.View>
    lateinit var adapter: FeedAdapter
    lateinit var goodreadsApi: GoodreadsAPI

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        async(UI) {
            presenter.showProgressDialog()
            val data = async { goodreadsApi.getHome() }.await()
            onDataFetched(data)
        }



        return inflater.inflate(R.layout.fragment_list, container, false)
    }

    override fun onDataFetched(data: FeedResponse) {
        adapter = FeedAdapter(presenter, data, goodreadsApi)
        list.adapter = adapter
        list.layoutManager = LinearLayoutManager(context)
        header_text.text = getString(R.string.home)
        presenter.dismissProgressDialog()
    }

//    override fun onBookClicked(isbn: String) {
//        (activity as HomeActivity).showBook(isbn)
//    }

    override fun onBookClicked(isbn: String) {
        (activity as HomeActivity).showBook(isbn)
    }

    override fun onUserClicked(id: String) {
        async(UI) {
            showProgressDialog()
            val data = async { goodreadsApi.getUser(id) }.await()
            dismissProgressDialog()
            (activity as HomeActivity).showFragment(ProfileFragment().apply { goodreadsApi = this@FeedFragment.goodreadsApi; userId = id }, R.string.fragment_profile)
        }

    }

    override fun onAttach(context: Context?) {
        AndroidSupportInjection.inject(this)
        presenter.onAttach(this)
        super.onAttach(context)
    }

    override fun onDetach() {
        super.onDetach()
        presenter.onDetach()
    }

}