package bg.devlabs.bookreviews.ui.base

import bg.devlabs.bookreviews.data.DataManager
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

/**
 * Created by Radoslav Yankov on 26.10.2017
 * Dev Labs
 * radoslavyankov@gmail.com
 */

open class BasePresenter<V : BaseContract.View> @Inject
constructor(val dataManager: DataManager, val mCompositeDisposable: CompositeDisposable) : BaseContract.Presenter<V> {

    lateinit var view: V
        private set

    override fun onAttach(view: V) {
        this.view = view
    }

    override fun onDetach() {
        mCompositeDisposable.dispose()
    }

    val isViewAttached: Boolean
        get() = true

    fun hideKeyboard() = view.hideKeyboard()

    override fun showProgressDialog() = view.showProgressDialog()

    override fun dismissProgressDialog() = view.dismissProgressDialog()

}