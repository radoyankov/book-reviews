package bg.devlabs.bookreviews.ui.bookshelves

import android.content.Context
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import bg.devlabs.bookreviews.R
import bg.devlabs.bookreviews.data.api.GoodreadsAPI
import bg.devlabs.bookreviews.data.api.xml.responses.UserResponse
import bg.devlabs.bookreviews.data.models.FullBook
import bg.devlabs.bookreviews.data.models.LocalBook
import bg.devlabs.bookreviews.ui.base.BaseActivity
import bg.devlabs.bookreviews.ui.base.BaseFragment
import bg.devlabs.bookreviews.ui.book.ProfileBookAdapter
import bg.devlabs.bookreviews.ui.home.HomeActivity
import bg.devlabs.bookreviews.utils.*
import dagger.android.support.AndroidSupportInjection
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import javax.inject.Inject

/**
 * Created by Radoslav Yankov on 03.05.2018
 * Dev Labs
 * radoslavyankov@gmail.com
 */
class ProfileFragment : BaseFragment(), ProfileContract.View {

    @Inject
    lateinit var presenter: ProfilePresenter<ProfileContract.View>
    lateinit var goodreadsApi: GoodreadsAPI
    var userId = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        async(UI) {
            presenter.showProgressDialog()
            val data = async {
                goodreadsApi.getUser(if (userId.isEmpty()) presenter.getUserId()
                        ?: "" else userId)
        }.await()
            onDataFetched(data)
        }

        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        local_books.onClick {
            showLocalBooks()
        }
        if (!context.hasNetwork()) {
            profile_image_card.visibility = View.INVISIBLE
            joined_label.visibility = View.GONE
            book_link.visibility = View.GONE
            textView.visibility = View.GONE
            showLocalBooks()
            delay(500) {
                (context as? BaseActivity)?.dismissProgressDialog()
            }
        }
    }

    override fun onDataFetched(data: UserResponse) {
        data.user.apply {
            profile_image.load(data.user.image_url)
            name_text.text = name
            location_text.text = location
            favorite_text.text = favorite_books + if (favorite_authors != "" && (favorite_authors?.contains("\n")) != true) (", " + favorite_authors) else ""
            joined_text.text = joined
            joined_text
            book_link.onClick {
                link?.let {
                    context?.openWeb(link ?: "")
                }
            }

            val transaction = activity?.supportFragmentManager?.beginTransaction()
            transaction?.add(R.id.bookshelves_frame, BookshelvesFragment().apply {
                goodreadsApi = this@ProfileFragment.goodreadsApi
                if (!this@ProfileFragment.userId.isEmpty()) {
                    userId = this@ProfileFragment.userId
                }
            }, getString(R.string.fragment_bookshelves_inner))
            transaction?.addToBackStack(getString(R.string.fragment_bookshelves_inner))
            transaction?.commit()
        }
        presenter.dismissProgressDialog()
    }

    private fun showLocalBooks() {
        if (local_books_list.alpha == 0f) {
            local_books_list.visibility = View.VISIBLE
            async {
                presenter.getLocals()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({
                            val adapter = ProfileBookAdapter(presenter, it)
                            local_books_list.animate().alpha(1f)
                            local_books_list.adapter = adapter
                            local_books_list.layoutManager = LinearLayoutManager(this@ProfileFragment.context,
                                    LinearLayoutManager.HORIZONTAL, false)

                        }, {
                            showToast(getString(R.string.db_error))
                        })
            }
        } else {
            local_books_list.animate().alpha(0f)
            delay(500) {
                local_books_list.visibility = View.GONE
            }
        }
    }

    override fun onBookClicked(book: LocalBook) {
        (activity as HomeActivity).setupViews(FullBook(book.id, book.title, book.image,
                book.description, book.rating, book.author, book.ratings_widget, book.link))
    }

    override fun onAttach(context: Context?) {
        AndroidSupportInjection.inject(this)
        presenter.onAttach(this)
        super.onAttach(context)
    }

    override fun onDetach() {
        super.onDetach()
        presenter.onDetach()
    }

}