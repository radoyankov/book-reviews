package bg.devlabs.bookreviews.ui.home

import android.Manifest
import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.view.View
import android.view.animation.DecelerateInterpolator
import bg.devlabs.bookreviews.R
import bg.devlabs.bookreviews.data.api.GoodreadsAPI
import bg.devlabs.bookreviews.data.api.auth.OAuthDialogFragment
import bg.devlabs.bookreviews.data.models.FullBook
import bg.devlabs.bookreviews.ui.base.BaseActivity
import bg.devlabs.bookreviews.ui.bookshelves.BookshelvesFragment
import bg.devlabs.bookreviews.ui.bookshelves.FeedFragment
import bg.devlabs.bookreviews.ui.bookshelves.ProfileFragment
import bg.devlabs.bookreviews.utils.*
import com.google.zxing.Result
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.layout_book.*
import kotlinx.android.synthetic.main.layout_onboarding.*
import kotlinx.coroutines.experimental.async
import me.dm7.barcodescanner.core.IViewFinder
import me.dm7.barcodescanner.zxing.ZXingScannerView
import javax.inject.Inject
import android.view.WindowManager

class HomeActivity : BaseActivity(), HomeContract.View, ZXingScannerView.ResultHandler, OAuthDialogFragment.AuthorizeListener {
    @JvmField
    @Inject
    var presenter: HomePresenter<HomeContract.View>? = null
    private var tutorialDelegate: TutorialDelegate? = null
    private lateinit var bookDelegate: BookDelegate
    private var goodreadsApi = GoodreadsAPI(this, ApiEventListener())

    private var scanner: ZXingScannerView? = null
    private var screenSize = 0f
    private var isBookOpened = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        AndroidInjection.inject(this)
        presenter?.onAttach(this)
        hideViews()
        tutorial()
        window.decorView.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.colorPrimaryLight))

    }

    /**
     * Hides the unnecessary views on app start
     */
    private fun hideViews() {
        screenSize = getScreenSize().second.toFloat()
        container.translationY = screenSize
        onboarding_container.visibility = View.GONE
        image_wrapper.alpha = 0f
        last_book.visibility = View.GONE
        container.alpha = 0f
        window.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)

        try {
            val shiftingMode = navigation.getChildAt(0).javaClass.getDeclaredField("mShiftingMode")
            shiftingMode.isAccessible = true
            shiftingMode.setBoolean(navigation.getChildAt(0), false)
            shiftingMode.isAccessible = false
        } catch (e: Exception) {
            //can't happen
        }
    }

    /**
     * Starts the tutorial process
     */
    private fun tutorial() = presenter?.checkTutorial()

    override fun skipTutorial() = setup()

    override fun showTutorial() {
        onboarding_container.visibility = View.VISIBLE
        navigation.visibility = View.INVISIBLE
        tutorialDelegate = TutorialDelegate(window.decorView, presenter)
    }

    override fun finishTutorial() {
        onboarding_container.animation {
            translationY(screenSize + 400)
            duration = mainLayoutAniDuration
        }
        delay(mainLayoutAniDuration) {
            onboarding_container.visibility = View.GONE
//            handlePermissions()
            setup()
        }
    }

    /**
     * Check for all required permissions
     */
    private fun handlePermissions() {
        requestPermission(Manifest.permission.CAMERA, Handler.Callback {
            if (it.data.getBoolean("permission")) {
                for (fragment in supportFragmentManager.fragments) {
                    supportFragmentManager.beginTransaction().remove(fragment).commit()
                }
//                setup()
//                bookDelegate.currentIsbn = "9780765317506"
//                presenter?.getBook("9780765317506", 1)
                setupScanner()
            }
            false
        })
    }

    /**
     * Initial setup after handling permissions
     */
    private fun setup() {
        setupNavigation()
        bookDelegate = BookDelegate(this, goodreadsApi, window.decorView, presenter)
    }

    private fun setupNavigation() {

        navigation.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.action_barcode -> {
                    showScan()
                    true
                }
                R.id.action_bookshelves -> {
                    showBookshelves()
                    true
                }
                R.id.action_feed -> {
                    showFeed()
                    true
                }
                R.id.action_profile -> {
                    showProfile()
                    true
            }
                else -> {
                    true
                }
            }
        }
        navigation.selectedItemId = R.id.action_bookshelves
    }

    private fun showProfile() {
        showFragment(ProfileFragment().apply { goodreadsApi = this@HomeActivity.goodreadsApi }, R.string.fragment_profile)
    }

    private fun showFeed() {
        showFragment(FeedFragment().apply {
            goodreadsApi = this@HomeActivity.goodreadsApi
        }, R.string.fragment_feed)
    }

    private fun showBookshelves() {
        showFragment(BookshelvesFragment().apply {
            goodreadsApi = this@HomeActivity.goodreadsApi
        }, R.string.fragment_bookshelves)
    }

    private fun showScan() {
        handlePermissions()
    }

    fun showFragment(fragment: Fragment, tagResId: Int) {
        stopCamera()
        showFragment(fragment, getString(tagResId))
    }

    private fun showFragment(fragment: Fragment, title: String) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.fragment_layout, fragment, title)
        transaction.addToBackStack(title)
        transaction.commit()
    }

    override fun showBook(isbn: String) {
        showProgressDialog()
        presenter?.getBook(isbn, 1)
    }

    /**
     * Sets up the scanner view
     */
    private fun setupScanner() {
        scanner = object : ZXingScannerView(applicationContext) {
            override fun createViewFinderView(context: Context?): IViewFinder = CustomViewFinderView(applicationContext)
        }
        scanner?.setResultHandler(this)
        fragment_layout.addView(scanner)
        startCamera()
    }

    override fun handleResult(result: Result?) {
        if (result != null && result.text != "") {
            bookDelegate.currentIsbn = result.text
            bookDelegate.reviewsPage = 1
            showToast(getString(R.string.book_found))
            presenter?.getBook(result.text, bookDelegate.reviewsPage)
            stopCamera()
        } else if (result?.text == "") {
            scanner?.resumeCameraPreview(this)
        }
    }

    override fun setupViews(book: FullBook) {
        dismissProgressDialog()
        fillViews(book)
        setupWebView(book)
    }

    private fun fillViews(book: FullBook) = bookDelegate.fillViews(book)
    override fun setupWebView(book: FullBook) = bookDelegate.setupWebView(book)

    override fun showPreviousButton() {
    }

    override fun startCamera() {
        scanner?.setResultHandler(this)
        scanner?.startCamera()
    }

    private fun stopCamera() = scanner?.stopCamera()

    /**
     * Shows the book drawer
     */
    override fun showBookDrawer() {
        isBookOpened = true
        container.apply {
            scrollTo(0, 0)
            alpha = 1f
            rotationX = 10f
            animation {
                alpha(1f)
                rotationX(0f)
                translationY(0f)
                interpolator = DecelerateInterpolator()
                duration = mainLayoutAniDuration
            }

        }
    }

    /**
     * Hides the book drawer
     */
    override fun hideBookDrawer() {
        isBookOpened = false
//        startCamera()
        presenter?.checkLastBook()
        delay(400) {
            bookDelegate.resetBook()
            container.animation {
                translationY(screenSize + 400)
                interpolator = DecelerateInterpolator()
                duration = mainLayoutAniDuration
            }
            delay(mainLayoutAniDuration) {
                container.animate().alpha(0f)
            }
        }
    }

    override fun onBackPressed() {
        if (isBookOpened) {
            hideBookDrawer()
        } else {
            super.onBackPressed()
        }
    }

    override fun showToast(message: String) {
        snackbar = Snackbar.make(fragment_layout, message, Snackbar.LENGTH_SHORT)
        snackbar?.show()
    }

    override fun handleLogin() {
        goodreadsApi.login(object : GoodreadsAPI.OAuthLoginCallback {

            override fun onSuccess() {
                showToast(getString(R.string.logged_in))
                async {
                    presenter?.saveUserId(goodreadsApi.getUserId())
                }

            }

            override fun onError(tr: Throwable) {
                showToast(getString(R.string.try_again))
            }
        })
    }

    override fun onAuthorized(token: String) {
        goodreadsApi.onAuthorized(token)
    }

    override fun onAuthorizeError() {
        goodreadsApi.onAuthorizeError()
    }

    inner class ApiEventListener : GoodreadsAPI.ApiEventListener {

        override fun onNeedsCredentials() {
            handleLogin()
        }

    }

}

