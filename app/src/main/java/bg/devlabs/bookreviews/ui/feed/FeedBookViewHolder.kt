package bg.devlabs.bookreviews.ui.bookshelves

import android.text.Html
import android.view.View
import bg.devlabs.bookreviews.data.api.GoodreadsAPI
import bg.devlabs.bookreviews.data.api.xml.objects.ObjectBook
import bg.devlabs.bookreviews.data.api.xml.objects.Update
import bg.devlabs.bookreviews.ui.feed.BaseViewHolder
import bg.devlabs.bookreviews.utils.load
import bg.devlabs.bookreviews.utils.loadCircle
import bg.devlabs.bookreviews.utils.onClick
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_update.*

/**
 * Created by Radoslav Yankov on 03.05.2018
 * Dev Labs
 * radoslavyankov@gmail.com
 */
class FeedBookViewHolder(val presenter: FeedContract.Presenter<FeedContract.View>,
                         override val containerView: View) : BaseViewHolder(containerView), LayoutContainer {

    override fun bind(entry: Update?, goodreadsApi: GoodreadsAPI) {
        book_image.load((entry?.image_url))
        book_image.onClick {
            try {
                presenter.fetchBook((entry?.obj?.obj as ObjectBook).id.toString())
            } catch (e: Exception) {
            }
        }
        book_title_text.text = ((entry?.obj?.obj as? ObjectBook)?.title)
        (entry?.obj?.obj as? ObjectBook)?.authors?.get(0)?.name?.let {
            if (it != "") {
                book_author_text.text = "by " + it
            }

        }
        status_text.text = Html.fromHtml(entry?.action_text)
        try {

            var stars = ""
            var leftOver = "★★★★★"
            for (i in 0..((entry?.action_text?.get(5)?.toString()?.toInt() ?: 0) - 1)) {
                stars += "★"
                leftOver = leftOver.dropLast(1)
            }
            book_extra_text.text = Html.fromHtml("<font color=\"#fccf1a\">$stars</font><font color=\"#5e5e5e\">$leftOver</font>")

        } catch (e: Exception) {
        }
        person_image.loadCircle(entry?.actor?.image_url)
        person_image.onClick { presenter.fetchUser(entry?.actor?.id.toString()) }
    }
}