package bg.devlabs.bookreviews.ui.book

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import bg.devlabs.bookreviews.R
import bg.devlabs.bookreviews.data.models.LocalBook
import bg.devlabs.bookreviews.ui.bookshelves.ProfileContract
import bg.devlabs.bookreviews.utils.inflate

/**
 * Created by Radoslav Yankov on 03.05.2018
 * Dev Labs
 * Adapter for showing the profile screen book list
 * radoslavyankov@gmail.com
 */
class ProfileBookAdapter(val presenter: ProfileContract.Presenter<ProfileContract.View>, val data: List<LocalBook>) : RecyclerView.Adapter<ProfileBookViewHolder>() {
//    val

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int) = ProfileBookViewHolder(presenter, parent?.inflate(R.layout.item_book))

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: ProfileBookViewHolder, position: Int) {
        holder.bind(data[position])
    }

}