package bg.devlabs.bookreviews.ui.bookshelves

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import bg.devlabs.bookreviews.data.api.GoodreadsAPI
import bg.devlabs.bookreviews.data.api.xml.objects.UserShelf
import bg.devlabs.bookreviews.ui.book.BookAdapter
import bg.devlabs.bookreviews.utils.onClick
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_bookshelf.*
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async

/**
 * Created by Radoslav Yankov on 03.05.2018
 * Dev Labs
 * Class for displaying an item in the bookshelf screen
 * radoslavyankov@gmail.com
 */
class BookshelfViewHolder(val presenter: BookshelvesContract.Presenter<BookshelvesContract.View>,
                          override val containerView: View?) : RecyclerView.ViewHolder(containerView), LayoutContainer {

    fun bind(userId: String, userShelf: UserShelf?, goodreadsApi: GoodreadsAPI) {
        async(UI) {
            val books = async {
                goodreadsApi.getShelf(if (userId.isEmpty()) presenter.getUserId() ?: "" else userId, userShelf?.name
                        ?: "read")
            }.await()

            val adapter = BookAdapter(presenter, books)
            list_books.adapter = adapter
            list_books.layoutManager = LinearLayoutManager(containerView?.context, LinearLayoutManager.HORIZONTAL, false)
            title_text.text = books.shelf.name.orEmpty()
            container.onClick {
            }
            if (books.reviews?.size == 0){
                container.visibility = View.INVISIBLE
            }
        }

    }
}