package bg.devlabs.bookreviews.ui.base

import android.app.ProgressDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Point
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.net.wifi.WifiManager
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.support.v4.app.Fragment
import android.support.v7.app.ActionBar
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.inputmethod.InputMethodManager
import bg.devlabs.bookreviews.R
import bg.devlabs.bookreviews.utils.Error
import com.afollestad.materialdialogs.MaterialDialog
import com.tbruyelle.rxpermissions2.RxPermissions

/**
 * Created by Radoslav Yankov on 03.05.2018
 * Dev Labs
 * radoslavyankov@gmail.com
 */

abstract class BaseFragment : Fragment(), BaseContract.View {

    fun showFragment(fragment: Fragment, titleResId: Int) {
        showFragment(fragment, getString(titleResId))
        hideKeyboard()
    }

    fun showFragment(fragment: Fragment, title: String) {
        val transaction = fragmentManager?.beginTransaction()
        transaction?.replace(R.id.fragment_layout, fragment, title)
        transaction?.addToBackStack(title)
        transaction?.commit()
        val actionBar = (activity as AppCompatActivity).supportActionBar ?: return
        actionBar.title = title.toUpperCase()
        toggleBackButtonVisibility(actionBar, true)
    }

    private fun toggleBackButtonVisibility(actionBar: ActionBar, visible: Boolean) {
        actionBar.setHomeButtonEnabled(visible)
        actionBar.setDisplayHomeAsUpEnabled(visible)
    }

    fun toggleBackButtonVisibility(visible: Boolean) {
        val actionBar = (activity as AppCompatActivity).supportActionBar ?: return
        toggleBackButtonVisibility(actionBar, visible)
    }

    var isNetworkConnected: Boolean = false
        get() {
            val connectivityManager = activity?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            return connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).state == NetworkInfo.State.CONNECTED || connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).state == NetworkInfo.State.CONNECTED
        }

    override fun showErrorDialog(message: String) {

        activity?.let {
            AlertDialog.Builder(it)
                .setTitle(R.string.dialog_error)
                .setMessage(message)
                .setPositiveButton(getString(R.string.dialog_ok), EmptyOnClickListener())
                .show()
        }
    }


    private class EmptyOnClickListener : DialogInterface.OnClickListener {
        override fun onClick(dialogInterface: DialogInterface, i: Int) {

        }
    }

    override fun showErrorDialog(messageResId: Int) {

        activity?.let {
            AlertDialog.Builder(it)
                .setTitle(R.string.dialog_error)
                .setMessage(messageResId)
                .setPositiveButton(R.string.dialog_ok, EmptyOnClickListener())
                .show()
        }
    }

    override fun showToast(messageResId: Error) = showToast(messageResId.name)

    override fun getApplicationContext() = activity?.applicationContext!!

    override fun hideKeyboard() {
        (activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(this.activity?.currentFocus?.windowToken, 0)
    }

    override fun getScreenSize(): Pair<Int, Int> {
        val display = activity?.windowManager?.defaultDisplay
        val size = Point()
        display?.getSize(size)
        return Pair(size.x, size.y)
    }

    override fun requestPermission(permission: String, callback: Handler.Callback) {
        activity?.let {
            RxPermissions(it).request(permission)
                .subscribe { isGranted ->
                    val bundle = Bundle(1)
                    val message = Message()
                    bundle.putBoolean("permission", isGranted)
                    message.data = bundle
                    callback.handleMessage(message)
                }
        }
    }

    /**
     * Shows a network error dialog. Opens wifi settings on accept
     */
    fun showNetworkError() {
        hideProgressDialog()
//        Log.d(TAG, "showNetworkError: 3")
        if (shouldShowNetworkError) {
            showDialog(getString(R.string.error_connection), getString(R.string.error_connection_prompt),
                    Runnable {
                        try {
                            startActivity(Intent(WifiManager.ACTION_PICK_WIFI_NETWORK))
                        } catch (ignored: IllegalStateException) {
                        }
                    }, Runnable { })
            shouldShowNetworkError = false
        }
    }

    private fun showDialog(title: String, desc: String, acceptFunction: Runnable,
                           rejectFunction: Runnable) {
        val context: Context? = context
        if (context != null) {
            AlertDialog.Builder(context)
                    .setTitle(title.toUpperCase())
                    .setMessage(desc)
                    .setPositiveButton(R.string.dialog_yes) { _, _ -> acceptFunction.run() }
                    .setNegativeButton(R.string.dialog_no) { _, _ -> rejectFunction.run() }
                    .create()
                    .show()
        }
    }


    fun hideProgressDialog() {
        if (dialog != null) {
            if (dialog?.isShowing == true)
                dialog?.hide()
        }
    }

    override fun showToast(text: String) {
        (activity as BaseActivity).showToast(text)
    }

    override fun showToast(resId: Int) {
        (activity as BaseActivity).showToast(resId)
    }

    fun showErrorToast() {
        showToast(getString(R.string.error_generic))
    }

    override fun shareText(text: String) {
        (activity as BaseActivity).shareText(text)
    }

    override fun openActivity(activityClass: Class<*>, names: Array<String>, values: Array<String>) {
        (activity as BaseActivity).openActivity(activityClass, names, values)
    }

    override fun showProgressDialog() {
        if (activity?.isFinishing == true) {
            return
        }

        (activity as BaseActivity).progressDialog?.let {
            if (!it.isShowing) {
                showProgressSimple()
            }
        } ?: run {
            showProgressSimple()
        }
    }

    private fun showProgressSimple() {
        (activity as BaseActivity).progressDialog = MaterialDialog.Builder(activity as BaseActivity)
                .title(getString(R.string.updating))
                .content(getString(R.string.please_wait))
                .progress(true, 0)
                .show()
    }

    override fun dismissProgressDialog() {
        if (activity?.isFinishing == true) {
            return
        }
        (activity as BaseActivity).progressDialog?.let {
            if (it.isShowing) {
                it.dismiss()
            }
        }
    }

    companion object {
        private val TAG = BaseFragment::class.java.simpleName
        internal var dialog: ProgressDialog? = null
        private var shouldShowNetworkError = true
    }
}
