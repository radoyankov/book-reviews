package bg.devlabs.bookreviews.ui.bookshelves

import bg.devlabs.bookreviews.data.DataManager
import bg.devlabs.bookreviews.ui.base.BasePresenter
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

/**
 * Created by Radoslav Yankov on 03.05.2018
 * Dev Labs
 * radoslavyankov@gmail.com
 */
class FeedPresenter<V : FeedContract.View> @Inject
constructor(dataManager: DataManager, compositeDisposable: CompositeDisposable) : BasePresenter<V>(dataManager, compositeDisposable), FeedContract.Presenter<V> {
    override fun fetchBook(isbn: String) {
        if (!isbn.isEmpty()) {
            view.onBookClicked(isbn)
        }
    }

    override fun fetchUser(id: String) {
        view.onUserClicked(id)
    }

}