package bg.devlabs.bookreviews.utils

import android.animation.ObjectAnimator
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Handler
import android.support.customtabs.CustomTabsIntent
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import bg.devlabs.bookreviews.R
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.request.RequestOptions
import android.net.NetworkInfo
import android.net.ConnectivityManager



/**
 * Created by Radoslav Yankov on 26.10.2017
 * Dev Labs
 * radoslavyankov@gmail.com
 */

//Loads an image from a url into a view
fun ImageView.load(url: String?) {
    val options = RequestOptions()
            .centerCrop()
    Glide.with(this.context)
            .load(url)
            .apply(options)
            .into(this)
}

//Loads an image from a url into a view with a circle effect
fun ImageView.loadCircle(url: String?) {
    val options = RequestOptions()
            .centerCrop()
            .circleCrop()
    Glide.with(this.context)
            .load(url)
            .apply(options)
            .into(this)
}

//Loads an image from a local id
fun ImageView.load(id: Int) {
    val options = RequestOptions()
            .priority(Priority.HIGH)
    Glide.with(this.context)
            .load(id)
            .apply(options)
            .into(this)
}

//Inflates a layout in a ViewGroup
fun ViewGroup.inflate(layout: Int): View {
    return LayoutInflater.from(this.context).inflate(layout, this, false)
}

fun Fragment.hideKeyboard() {
    val inputMethodManager: InputMethodManager =
            context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    inputMethodManager.hideSoftInputFromWindow(view?.windowToken, 0)
}

//Sets the tint of an imageView
fun ImageView.setTint(colorId: Int) {
    this.setColorFilter(ContextCompat.getColor(context, colorId), android.graphics.PorterDuff.Mode.SRC_ATOP)

}

//Checks for network connection
fun Context.hasNetwork(): Boolean {
    val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    return connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).state == NetworkInfo.State.CONNECTED ||
            connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).state == NetworkInfo.State.CONNECTED
}

//Animates a view iwth a custom func
fun View.animation(func: ViewPropertyAnimator.() -> Unit) {
    this.animate().func()
}

//Opens a web browser with a url
fun Context.openWeb(url: String, context: Context? = null) {
    if (context != null) {
        val builder = CustomTabsIntent.Builder()
        builder.setToolbarColor(context.resources.getColor(R.color.colorPrimary))
        val customTabsIntent = builder.build()
        customTabsIntent.launchUrl(context, Uri.parse(url))
    } else {
        startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url)))
    }
}

//Delays the execution of a function
fun delay(delay: Long, func: () -> Unit) {
    Handler().postDelayed({
        func()
    }, delay)
}

//Easy on click handling
fun View.onClick(func: (() -> Unit)?) {
    this.setOnClickListener {
        func?.let { it1 -> it1() }
    }
}

fun View.removeOnClick() {
    this.setOnClickListener { }
}

//rotates a view (a button mostly) and changes the text inside
fun View.rotateAndChange(func: () -> Unit) {
    val halfAnimationDuration = 200L
    ObjectAnimator.ofFloat(this, "rotationX", 0f, 90f).apply {
        duration = halfAnimationDuration
        start()
    }

    delay(halfAnimationDuration) {
        func()
        ObjectAnimator.ofFloat(this, "rotationX", -90f, 0f).apply {
            duration = halfAnimationDuration
            start()
        }
    }
}

object AnimatedOnTouch : View.OnTouchListener {
    override fun onTouch(view: View, motionEvent: MotionEvent): Boolean {
        when (motionEvent.action) {
            MotionEvent.ACTION_DOWN -> {
                view.animation {
                    scaleY(1.05f)
                    scaleX(1.05f)
                    duration = 100
                }
            }
            MotionEvent.ACTION_CANCEL, MotionEvent.ACTION_UP -> {
                view.animation {
                    scaleY(1f)
                    scaleX(1f)
                    duration = 100
                }
            }
        }

        return false
    }

}