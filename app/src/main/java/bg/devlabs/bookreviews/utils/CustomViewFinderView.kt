package bg.devlabs.bookreviews.utils

import android.content.Context
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.util.TypedValue
import me.dm7.barcodescanner.core.ViewFinderView


/**
 * Created by Radoslav Yankov on 27.10.2017
 * Dev Labs
 * radoslavyankov@gmail.com
 */
class CustomViewFinderView : ViewFinderView {
    private val paint = Paint()

    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init()
    }

    private fun init() {
        paint.color = Color.WHITE
        paint.isAntiAlias = true
        val textPixelSize = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP,
                TRADE_MARK_TEXT_SIZE_SP.toFloat(), resources.displayMetrics)
        paint.textSize = textPixelSize
        setSquareViewFinder(true)
    }

    companion object {
        val TRADE_MARK_TEXT = "ZXing"
        val TRADE_MARK_TEXT_SIZE_SP = 40
    }
}
