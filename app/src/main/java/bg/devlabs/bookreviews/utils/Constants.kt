package bg.devlabs.bookreviews.utils


/**
 * Created by Radoslav Yankov on 26.10.2017
 * Dev Labs
 * radoslavyankov@gmail.com
 */

//default values for missing objects on screens
const val defaultTitle = "Title"
const val defaultImage = "https://s.gr-assets.com/assets/nophoto/user/f_50x66-6a03a5c12233c941481992b82eea8d23.png"
const val defaultDescription = ""
const val defualtRating = "0.0"
const val defaultAuthor = "Author"
const val defaultReviews = ""

const val defaultLink = "https://www.goodreads.com"
const val consumerKey = "06gj4AUw6VxR9x8GxmcYBA"
const val consumerSecret = "t5HM4hhwByWnZ9pQRUurzHbgZCnJE1xZeajJzOqe7g"

//enum for error types
enum class Error {
    BOOK_NOT_FOUND,
    OTHER_ERROR
}

//constants for animation durations in the book screen
const val mainLayoutAniDuration = 700L
const val mainLayoutAniDelay = 0L
const val bookCoverAniDuration = 700L
const val bookCoverAniDelay = 900L
const val webViewAniDuration = 200L
const val webViewAniDelay = 0L
const val starAniDelay = mainLayoutAniDuration + 900L