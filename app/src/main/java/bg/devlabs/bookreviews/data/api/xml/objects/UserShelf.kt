package bg.devlabs.bookreviews.data.api.xml.objects

import org.simpleframework.xml.Element
import org.simpleframework.xml.Root

/**
 * Created by Radoslav Yankov on 02.05.2018
 * Dev Labs
 * radoslavyankov@gmail.com
 */
@Root(name = "user_shelf", strict = false)
data class UserShelf @JvmOverloads constructor(
        @field:Element(name = "id", required = false)
        @param:Element(name = "id", required = false)
        var id: Int?,
        @field:Element(name = "user_id", required = false)
        @param:Element(name = "user_id", required = false)
        var user_id: Int?,
        @field:Element(name = "name", required = false)
        @param:Element(name = "name", required = false)
        var name: String?,
        @field:Element(name = "book_count", required = false)
        @param:Element(name = "book_count", required = false)
        var book_count: Int?
)
//val updated_at = ""                 type="datetime">2018-05-02T08:38:50+00:00
//val created_at = ""                 type="datetime">2018-05-02T08:38:50+00:00
