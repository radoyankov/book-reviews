package bg.devlabs.bookreviews.data.prefs

import bg.devlabs.bookreviews.data.models.FullBook


/**
 * Created by Radoslav Yankov on 26.10.2017
 * Dev Labs
 * radoslavyankov@gmail.com
 */

interface PreferencesHelper{
    /**
     * Flag for whether the user has passed the onboarding sequence
     */
    var onboardingPassed: Boolean

    /**
     * Object containing the user's last book
     */
    var lastBook: FullBook?

    /**
     * the user's id as String
     */
    var userId: String?
}