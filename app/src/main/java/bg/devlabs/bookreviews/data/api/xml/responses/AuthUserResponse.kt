package bg.devlabs.bookreviews.data.api.xml.responses

import org.simpleframework.xml.Element
import org.simpleframework.xml.Root

import bg.devlabs.bookreviews.data.api.xml.objects.AuthUser

@Root(name = "GoodreadsResponse", strict = false)
class AuthUserResponse {

    @Element
    val authUser: AuthUser? = null
}