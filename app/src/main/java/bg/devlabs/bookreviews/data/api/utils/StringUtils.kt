package bg.devlabs.bookreviews.data.api.utils

object StringUtils {

    fun isEmpty(str: String?): Boolean {
        return str == null || str.trim { it <= ' ' }.isEmpty()
    }

    fun isNotEmpty(str: String?): Boolean {
        return str != null && str.trim { it <= ' ' }.isNotEmpty()
    }

}
