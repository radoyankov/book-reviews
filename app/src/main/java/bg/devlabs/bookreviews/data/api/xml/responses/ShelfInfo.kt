package bg.devlabs.bookreviews.data.api.xml.responses

import org.simpleframework.xml.Attribute
import org.simpleframework.xml.Root

@Root(name = "shelf", strict = false)
data class ShelfInfo @JvmOverloads constructor(
        @param:Attribute(name = "name", required = false)
        @field:Attribute(name = "name", required = false)
        val name: String? = null,
        @param:Attribute(name = "id", required = false)
        @field:Attribute(name = "id", required = false)
        val id: String? = null
)