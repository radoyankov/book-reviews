package bg.devlabs.bookreviews.data.api.xml.responses

import bg.devlabs.bookreviews.data.api.xml.objects.ShelfReview
import org.simpleframework.xml.Element
import org.simpleframework.xml.ElementList
import org.simpleframework.xml.Root

/**
 * Created by Radoslav Yankov on 04.05.2018
 * Dev Labs
 * radoslavyankov@gmail.com
 */
@Root(name = "GoodreadsResponse", strict = false)
class ShelfResponse(
        @param:ElementList(name = "reviews")
        @field:ElementList(name = "reviews")
        var reviews: List<ShelfReview>?,
        @param:Element(name = "shelf")
        @field:Element(name = "shelf")
        var shelf: ShelfInfo

)