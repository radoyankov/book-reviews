package bg.devlabs.bookreviews.data.api.xml.objects

import org.simpleframework.xml.Element
import org.simpleframework.xml.ElementList
import org.simpleframework.xml.Root

@Root(name = "book", strict = false)
data class Book(

        @param:Element(name = "id", required = false)
        @field:Element(name = "id", required = false)
        val id: Int? = 0,

        @param:Element(name = "isbn", required = false)
        @field:Element(name = "isbn", required = false)
        val isbn: String? = null,

        @param:Element(name = "isbn13", required = false)
        @field:Element(name = "isbn13", required = false)
        val isbn13: String? = null,

        @param:Element(name = "text_reviews_count", required = false)
        @field:Element(name = "text_reviews_count", required = false)
        val textReviewsCount: Int? = 0,

        @param:Element(name = "title", required = false)
        @field:Element(name = "title", required = false)
        val title: String? = null,

        @param:Element(name = "image_url", data = true, required = false)
        @field:Element(name = "image_url", data = true, required = false)
        val imageUrl: String? = null,

        @param:Element(name = "small_image_url", data = true, required = false)
        @field:Element(name = "small_image_url", data = true, required = false)
        val smallImageUrl: String? = null,

        @param:Element(name = "link", required = false)
        @field:Element(name = "link", required = false)
        val link: String? = null,

        @param:Element(name = "num_pages", required = false)
        @field:Element(name = "num_pages", required = false)
        val numPages: Int? = 0,

        @param:Element(name = "format", required = false)
        @field:Element(name = "format", required = false)
        val format: String? = null,

        @param:Element(name = "edition_information", required = false)
        @field:Element(name = "edition_information", required = false)
        val editionInformation: String? = null,

        @param:Element(name = "publisher", required = false)
        @field:Element(name = "publisher", required = false)
        var publisher: String? = null,

        @param:Element(name = "publication_day", required = false)
        @field:Element(name = "publication_day", required = false)
        val publicationDay: Int? = 0,

        @param:Element(name = "publication_year", required = false)
        @field:Element(name = "publication_year", required = false)
        val publicationYear: Int? = 0,

        @param:Element(name = "publication_month", required = false)
        @field:Element(name = "publication_month", required = false)
        val publicationMonth: Int? = 0,

        @param:Element(name = "average_rating", required = false)
        @field:Element(name = "average_rating", required = false)
        val averageRating: Float? = 0.toFloat(),

        @param:Element(name = "ratings_count", required = false)
        @field:Element(name = "ratings_count", required = false)
        val ratingsCount: Int? = 0,

        @param:Element(name = "description", required = false)
        @field:Element(name = "description", required = false)
        val description: String? = null,

        @param:ElementList(name = "authors", required = false)
        @field:ElementList(name = "authors", required = false)
        val authors: List<AuthorInternal>? = null,

        @param:Element(name = "author", required = false)
        @field:Element(name = "author", required = false)
        val author: AuthorInternal? = null,

        @param:Element(name = "published", required = false)
        @field:Element(name = "published", required = false)
        val published: String? = null

//    /**
//     *
//     * @return
//     */
//    fun getAuthors(): List<Int> {
//        if (authors == null) {
//            return emptyList()
//        }
//
//        val ret = ArrayList<Int>(authors.size)
//        for (authorInternal in authors) {
//            ret.add(authorInternal.id)
//        }
//
//        return Collections.unmodifiableList(ret)
//    }
//
)

@Root(strict = false)
data class AuthorInternal @JvmOverloads constructor(
        @param:Element(name = "id", required = false)
        @field:Element(name = "id", required = false)
        internal var id: Int? = 0,
        @param:Element(name = "name", required = false)
        @field:Element(name = "name", required = false)
        internal var name: String? = null
)
