package bg.devlabs.bookreviews.data.api.xml.responses

import bg.devlabs.bookreviews.data.api.xml.objects.User
import org.simpleframework.xml.Element
import org.simpleframework.xml.Root

/**
 * Created by Radoslav Yankov on 02.05.2018
 * Dev Labs
 * radoslavyankov@gmail.com
 */
@Root(name = "GoodreadsResponse", strict = false)
data class UserResponse @JvmOverloads constructor(
    @param:Element(name = "user", required = false)
    @field:Element(name = "user", required = false)
    val user: User
)