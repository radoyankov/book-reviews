package bg.devlabs.bookreviews.data.api.xml.objects

import org.simpleframework.xml.Attribute
import org.simpleframework.xml.Element
import org.simpleframework.xml.Root

@Root(name = "action", strict = false)
data class Action @JvmOverloads constructor(
        @field:Attribute(name = "type", required = false)
        @param:Attribute(name = "type", required = false)
        val type: String? = null,
        @field:Element(name = "rating", required = false)
        @param:Element(name = "rating", required = false)
        val rating: Int? = null
)
