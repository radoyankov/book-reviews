package bg.devlabs.bookreviews.data

import bg.devlabs.bookreviews.data.models.FullBook
import bg.devlabs.bookreviews.data.network.ApiHelper
import bg.devlabs.bookreviews.data.prefs.PreferencesHelper
import io.reactivex.Observable

/**
 * Created by Radoslav Yankov on 26.10.2017
 * Dev Labs
 * radoslavyankov@gmail.com
 */
interface DataManager : ApiHelper, PreferencesHelper {
    /**
     * Gets a [FullBook] object with a given ISBN and page
     */
    fun getBook(isbn: String, page: Int): Observable<FullBook>
}

