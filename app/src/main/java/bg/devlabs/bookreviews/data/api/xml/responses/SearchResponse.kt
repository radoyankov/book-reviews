package bg.devlabs.bookreviews.data.api.xml.responses

import org.simpleframework.xml.Element
import org.simpleframework.xml.Root

/**
 * Created by Radoslav Yankov on 03.05.2018
 * Dev Labs
 * radoslavyankov@gmail.com
 */
@Root(name = "GoodreadsResponse", strict = false)
data class SearchResponse @JvmOverloads constructor(
        @param:Element(name = "search", required = false)
        @field:Element(name = "search", required = false)
            val search: Search
)