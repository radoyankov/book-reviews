package bg.devlabs.bookreviews.data.api.xml.objects

import org.simpleframework.xml.Default
import org.simpleframework.xml.Element
import org.simpleframework.xml.Root

@Root(name = "review", strict = false)
@Default
data class Review @JvmOverloads constructor(
        @param:Element(name = "id", required = false)
        @field:Element(name = "id", required = false)
        val id: String?,
        @param:Element(name = "user_id", required = false)
        @field:Element(name = "user_id", required = false)
        val user_id: Int?,
        @param:Element(name = "book_id", required = false)
        @field:Element(name = "book_id", required = false)
        val book_id: Int?,
        @param:Element(name = "rating", required = false)
        @field:Element(name = "rating", required = false)
        val rating: Int?,
        @param:Element(name = "read_status", required = false)
        @field:Element(name = "read_status", required = false)
        val read_status: String?,
        @param:Element(name = "review", required = false)
        @field:Element(name = "review", required = false)
        val review: String?,
        @param:Element(name = "recommendation", required = false)
        @field:Element(name = "recommendation", required = false)
        val recommendation: String?,
        @param:Element(name = "read_at", required = false)
        @field:Element(name = "read_at", required = false)
        val read_at: String?,
        @param:Element(name = "updated_at", required = false)
        @field:Element(name = "updated_at", required = false)
        val updated_at: String?,
        @param:Element(name = "created_at", required = false)
        @field:Element(name = "created_at", required = false)
        val created_at: String?,
        @param:Element(name = "comments_count", required = false)
        @field:Element(name = "comments_count", required = false)
        val comments_count: Int?,
        @param:Element(name = "weight", required = false)
        @field:Element(name = "weight", required = false)
        val weight: Int?,
        @param:Element(name = "ratings_sum", required = false)
        @field:Element(name = "ratings_sum", required = false)
        val ratings_sum: Int?,
        @param:Element(name = "ratings_count", required = false)
        @field:Element(name = "ratings_count", required = false)
        val ratings_count: Int?,
        @param:Element(name = "notes", required = false)
        @field:Element(name = "notes", required = false)
        val notes: String?,
        @param:Element(name = "spoiler_flag", required = false)
        @field:Element(name = "spoiler_flag", required = false)
        val spoiler_flag: Boolean?,
        @param:Element(name = "book", required = false)
        @field:Element(name = "book", required = false)
        val book: Book?
        )