package bg.devlabs.bookreviews.data.api

import android.app.Activity
import android.content.SharedPreferences
import android.os.AsyncTask
import android.text.TextUtils
import android.util.Log
import bg.devlabs.bookreviews.BuildConfig
import bg.devlabs.bookreviews.R

import org.apache.http.NameValuePair
import org.apache.http.client.entity.UrlEncodedFormEntity
import org.apache.http.client.methods.HttpGet
import org.apache.http.client.methods.HttpPost
import org.apache.http.client.utils.URLEncodedUtils
import org.apache.http.impl.client.DefaultHttpClient
import org.apache.http.message.BasicNameValuePair

import java.util.ArrayList
import java.util.HashMap

import bg.devlabs.bookreviews.data.api.auth.OAuthDialogFragment
import bg.devlabs.bookreviews.data.api.auth.OAuthLoginDialogType
import bg.devlabs.bookreviews.data.api.utils.StringUtils
import bg.devlabs.bookreviews.data.api.utils.UIUtils
import bg.devlabs.bookreviews.data.api.xml.objects.UserShelf
import bg.devlabs.bookreviews.data.api.xml.responses.*
import bg.devlabs.bookreviews.ui.home.HomeActivity
import kotlinx.coroutines.experimental.async
import oauth.signpost.commonshttp.CommonsHttpOAuthConsumer
import oauth.signpost.commonshttp.CommonsHttpOAuthProvider
import org.apache.commons.io.IOUtils
import org.simpleframework.xml.core.Persister

class GoodreadsAPI(private val activity: Activity, private val listener: ApiEventListener?) : OAuthDialogFragment.AuthorizeListener {
    var consumer: CommonsHttpOAuthConsumer? = null
        private set
    private var provider: CommonsHttpOAuthProvider? = null
    private var oAuthToken: String? = null
    private var oAuthCallbackUrl: String? = null
    private var oAuthLoginDialogType: OAuthLoginDialogType? = null

    var isLoggedIn = false
        private set

    private var oAuthLoginCallback: OAuthLoginCallback? = null

    private val sharedPrefs: SharedPreferences
        get() = activity.getSharedPreferences(SHARED_PREF_FILENAME, Activity.MODE_PRIVATE)

    interface OAuthLoginCallback {
        fun onSuccess()

        fun onError(tr: Throwable)
    }

    interface ApiEventListener {

        fun onNeedsCredentials()
    }

    init {

        // Set a default, can be changed by user
        oAuthLoginDialogType = if (UIUtils.isSmallestWidthGreaterThan600dp(activity))
            OAuthLoginDialogType.DIALOG
        else
            OAuthLoginDialogType.FULLSCREEN
    }

    fun requestIfLoggedIn(func: () -> Unit) {
        async {
            val oauthDeveloperKey = activity.applicationContext.getString(R.string.oauth_developer_key)
            val oauthDeveloperSecret = activity.applicationContext.getString(R.string.oauth_developer_secret)
            val oauthCallbackUrl = activity.applicationContext.getString(R.string.oauth_callback_url)

            setOAuthInfo(oauthDeveloperKey, oauthDeveloperSecret, oauthCallbackUrl)
            setOAuthLoginDialogType(OAuthLoginDialogType.FULLSCREEN)

            if (isLoggedIn) {
                func()
            } else {
                listener?.onNeedsCredentials()
            }
        }
    }

    fun setOAuthInfo(oAuthDeveloperKey: String, oAuthDeveloperSecret: String, oAuthCallbackUrl: String) {
        this.oAuthCallbackUrl = oAuthCallbackUrl

        if (TextUtils.isEmpty(oAuthDeveloperSecret) || TextUtils.isEmpty(oAuthDeveloperSecret)
                || TextUtils.isEmpty(this.oAuthCallbackUrl)) {
            val exception = "None may be empty: oAuthDeveloperKey, oAuthDeveloperSecret, oAuthCallbackUrl."
            Log.e(TAG, exception)
            throw RuntimeException(exception)
        }

        consumer = CommonsHttpOAuthConsumer(oAuthDeveloperKey, oAuthDeveloperSecret)
        provider = CommonsHttpOAuthProvider(REQUEST_TOKEN_ENDPOINT_URL, ACCESS_TOKEN_ENDPOINT_URL,
                AUTHORIZATION_WEBSITE_URL)

        // Check if we have a token
        val sp = sharedPrefs

        val token = sp.getString(USER_TOKEN, null)
        val tokenSecret = sp.getString(USER_SECRET, null)

        isLoggedIn = if (StringUtils.isNotEmpty(token) && StringUtils.isNotEmpty(tokenSecret)) {
            consumer?.setTokenWithSecret(token, tokenSecret)
            true
        } else {
            false
        }
    }

    fun setOAuthLoginDialogType(oAuthLoginDialogType: OAuthLoginDialogType) {
        this.oAuthLoginDialogType = oAuthLoginDialogType
    }

    fun login(callback: OAuthLoginCallback) {
        oAuthLoginCallback = callback
        (activity as HomeActivity).dismissProgressDialog()
        (activity).showToast("Login required")
        RetrieveRequestTokenTask().execute()
    }

    /**
     * A request function for executing GET requests
     *  @param service - the String for the service to be executed
     *  @param params - map of parameters to be added to the url
     *  @return the return result as a json string
     */
    @Throws(Exception::class)
    private fun request(service: String, params: Map<String, String>? = null): String? {
        val output: String

        // Create the request URL
        val url = StringBuilder(API_URL).append(service)

        // Create the set of request parameters, starting with developer key
        val requestParams = ArrayList<NameValuePair>()
        if (oAuthToken != null) {
            requestParams.add(BasicNameValuePair(KEY, oAuthToken))
        } else {
            requestParams.add(BasicNameValuePair(KEY, BuildConfig.API_KEY))
        }

        // If params have been passed in, add them to the request params now
        if (params != null && !params.isEmpty()) {
            for ((key, value) in params) {
                if (StringUtils.isNotEmpty(key) && value != null) {
                    requestParams.add(BasicNameValuePair(key, value))
                }
            }
        }

        // Add the encoded params to the request URL
        if (!requestParams.isEmpty()) {
            if (url[url.length - 1] != '?') {
                url.append('?')
            }
            url.append(URLEncodedUtils.format(requestParams, "UTF-8"))
        }

        val get = HttpGet(url.toString())

        if (service != CALL_GET_SHELF) {
            consumer?.sign(get)
        }
        val httpClient = DefaultHttpClient()
        val response = httpClient.execute(get)
        val stream = response.entity.content

        val statusCode = response.statusLine.statusCode
        Log.d(TAG, "status code = " + statusCode)
        // Log.d(TAG, "output = " + output);

        if (statusCode == 401) {
            clearAuthInformation()

            listener?.onNeedsCredentials()
        }

        return IOUtils.toString(stream, "UTF-8")
    }

    /**
     * A request function for executing POST requests
     *  @param service - the String for the service to be executed
     *  @param params - map of parameters to be added to the url
     *  @return the return result as a json string
     */
    @Throws(Exception::class)
    private fun postRequest(service: String, params: Map<String, String>?): String {

        // Create the request URL
        val url = StringBuilder(API_URL).append(service)

        // Create the set of request parameters, starting with developer key
        val requestParams = ArrayList<NameValuePair>()
        if (oAuthToken != null) {
            requestParams.add(BasicNameValuePair(KEY, oAuthToken))
        } else {
            requestParams.add(BasicNameValuePair(KEY, BuildConfig.API_KEY))
        }

        // If params have been passed in, add them to the request params now
        if (params != null && !params.isEmpty()) {
            for ((key, value) in params) {
                if (StringUtils.isNotEmpty(key) && value != null) {
                    requestParams.add(BasicNameValuePair(key, value))
                }
            }
        }

        // Add the encoded params to the request URL
        //		if (!requestParams.isEmpty()) {
        //			if (url.charAt(url.length() - 1) != '?') {
        //				url.append('?');
        //			}
        //			url.append(URLEncodedUtils.format(requestParams, "UTF-8"));
        //		}

        val post = HttpPost(url.toString())
        post.entity = UrlEncodedFormEntity(requestParams, "UTF-8")

        consumer?.sign(post)
        val httpClient = DefaultHttpClient()
        val response = httpClient.execute(post)
        val content = response.entity.content

        val statusCode = response.statusLine.statusCode
        Log.d(TAG, "status code = $statusCode")
        // Log.d(TAG, "output = " + output);

        if (statusCode == 401) {
            clearAuthInformation()

            listener?.onNeedsCredentials()
        }

        return IOUtils.toString(content, "UTF-8")
    }

    /**
     * function which executes when the user is authorized
     */
    override fun onAuthorized(token: String) {
        RetrieveAccessTokenTask().execute()
    }

    /**
     * function which executes when there is an auth error
     */
    override fun onAuthorizeError() {}

    private inner class RetrieveRequestTokenTask : AsyncTask<Void, Void, String>() {

        override fun doInBackground(vararg params: Void): String? {
            var authorizeUrl: String? = null
            try {
                authorizeUrl = provider?.retrieveRequestToken(consumer, oAuthCallbackUrl)
            } catch (e: Exception) {
                Log.e(TAG, "Exception while retrieving request token.", e)
                if (oAuthLoginCallback != null) {
                    oAuthLoginCallback?.onError(e)
                }
            }

            return authorizeUrl
        }

        override fun onPostExecute(authorizeUrl: String?) {
            if (authorizeUrl != null) {

                val f = OAuthDialogFragment.newInstance(authorizeUrl, oAuthCallbackUrl)

                if (oAuthLoginDialogType === OAuthLoginDialogType.FULLSCREEN) {
                    val ft = activity.fragmentManager.beginTransaction()
                    ft.add(android.R.id.content, f)
                    ft.commit()
                } else {
                    f.show(activity.fragmentManager, OAuthDialogFragment.TAG)
                }

                saveRequestInformation(consumer?.token ?: "", consumer?.tokenSecret ?: "")
            }
        }
    }

    private inner class RetrieveAccessTokenTask : AsyncTask<String, Void, Boolean>() {

        override fun doInBackground(vararg params: String): Boolean? {
            return try {
                provider?.retrieveAccessToken(consumer, if (params == null || params.isEmpty()) null else params[0])
                true
            } catch (e: Exception) {
                Log.e(TAG, "Exception while retrieving access token.", e)
                if (oAuthLoginCallback != null) {
                    oAuthLoginCallback?.onError(e)
                }
                false
            }
        }

        override fun onPostExecute(success: Boolean?) {
            isLoggedIn = success ?: false

            if (success == true) {
                oAuthToken = consumer?.token
                val oAuthTokenSecret = consumer?.tokenSecret
                consumer?.setTokenWithSecret(oAuthToken, oAuthTokenSecret)

                saveAuthInformation(oAuthToken, oAuthTokenSecret ?: "")
                clearRequestInformation()

                if (oAuthLoginCallback != null) {
                    oAuthLoginCallback?.onSuccess()
                }
            }
        }
    }

    private fun saveAuthInformation(token: String?, secret: String) {
        sharedPrefs.edit().apply {
            putString(USER_TOKEN, token)
            putString(USER_SECRET, secret)
            apply()
        }
    }

    private fun clearAuthInformation() {
        sharedPrefs.edit().apply {
            remove(USER_TOKEN)
            remove(USER_SECRET)
            apply()
        }

    }

    private fun saveRequestInformation(token: String, secret: String) {
        sharedPrefs.edit().apply {
            putString(REQUEST_TOKEN, token)
            putString(REQUEST_SECRET, secret)
            apply()
        }

    }

    private fun clearRequestInformation() {
        sharedPrefs.edit().apply {
            remove(REQUEST_TOKEN)
            remove(REQUEST_SECRET)
            apply()
        }
    }

    /**
     * Add to shelf call to add a book to shelf
     * Requires logged in user
     */
    fun addToShelf(bookId: String, toRemove: Boolean, shelf: String): UserShelf? {
        val hm = HashMap<String, String>()
        hm["name"] = shelf
        hm["book_id"] = bookId
        if (toRemove) {
            hm["a"] = "remove"
        }
        val response = postRequest(CALL_ADD_TO_SHELF, hm)

        return Persister().read(UserShelf::class.java, response)
    }

    /**
     * Create a shelf call to add a new shelf
     * Requires logged in user
     */
    fun createShelf(name: String): UserShelf {
        val response = postRequest(CALL_CREATE_SHELF, mapOf(Pair("v", "2"), Pair("user_shelf[name]", name)))
        Log.d("response createShelf: ", response)
        return Persister().read(UserShelf::class.java, response)
    }

    /**
     * Create a new review
     * Requires logged in user
     */
    fun createReview(bookId: String, review: String, rating: String): String {
        val response = postRequest(CALL_CREATE_REVIEW, mapOf(Pair("book_id", bookId)
                ,
                Pair("review[review]", review),
                Pair("review[rating]", rating)
        ))
        Log.d("response createReview: ", response)

        return ""
    }

    /**
     * Gets the user from a given id
     */
    fun getUser(id: String): UserResponse {
        val response = request("$CALL_GET_USER$id.xml")
        Log.d("response getUser: ", response)

        return Persister().read(UserResponse::class.java, response)
    }

//    fun getShelves(userId: String): String {
//        val response = request(CALL_GET_SHELVES, mutableMapOf(Pair("user_id", userId)))
//        Log.d("response ${Object().`class`.enclosingMethod.name}: ", response)
//
//        return ""
//    }

    /**
     * Gets a shelf from a given userId and a shelf String
     */
    fun getShelf(userId: String, shelf: String, query: String? = null, page: String? = null): ShelfResponse {
        //also can be used for search
        val response = request(CALL_GET_SHELF, mutableMapOf(Pair("v", "2"), Pair("id", userId), Pair("shelf", shelf)).apply {
            query?.let {
                put("search[query]", it)
            }
            page?.let {
                put("page", it)
            }
        })
        Log.d("response getShelf: ", response)
        //returns an html
        return Persister().read(ShelfResponse::class.java, response)
    }

    /**
     * Gets the home updates of a user
     * Requires logged in user
     */
    fun getHome(): FeedResponse {
        val response = request(CALL_FEED)
        Log.d("response getHome: ", response)

        return Persister().read(FeedResponse::class.java, response)
    }

    /**
     * Gets the current user id String
     * Requires logged in user
     */
    fun getUserId(): String {
        val response = request(CALL_GET_USER_ID)
        Log.d("response getUserId: ", response)
        return Persister().read(UserIdResponse::class.java, response).user.id
//        return Persister().read(FeedResponse::class.java, response)
    }

    /**
     * Performs a search query
     */
    fun search(query: String): SearchResponse {
        val response = request(CALL_SEARCH, mutableMapOf(Pair("q", query)))
        Log.d("response search: ", response)
        return Persister().read(SearchResponse::class.java, response)
//        return Persister().read(FeedResponse::class.java, response)
    }

    /**
     * Returns the Goodreads ID of a given book identified by it's ISBN.
     *
     * @param isbn
     * The book identified by an ISBN
     * @return The Goodreads ID of the book
     */
    fun getIsbnToId(isbn: String): String? {
        val response = request("book/isbn_to_id/$isbn")
        Log.d("response getIsbnToId: ", response)

        return ""
    }

    companion object {

        private const val KEY = "key"
        private val API_URL = "https://www.goodreads.com/"

        private const val AUTHORIZATION_WEBSITE_URL = "https://www.goodreads.com/oauth/authorize?mobile=1"
        private const val ACCESS_TOKEN_ENDPOINT_URL = "https://www.goodreads.com/oauth/access_token"
        private val REQUEST_TOKEN_ENDPOINT_URL = "https://www.goodreads.com/oauth/request_token"

        private val SHARED_PREF_FILENAME = GoodreadsAPI::class.java.`package`.name
        const val USER_TOKEN = "USER_TOKEN"
        const val USER_SECRET = "USER_SECRET"
        const val REQUEST_TOKEN = "REQUEST_TOKEN"
        const val REQUEST_SECRET = "REQUEST_SECRET"

        const val CALL_FEED = "updates/friends.xml"
        const val CALL_CREATE_SHELF = "user_shelves.xml"
        const val CALL_CREATE_REVIEW = "review.xml"
        const val CALL_ADD_TO_SHELF = "shelf/add_to_shelf.xml"
        const val CALL_GET_SHELF = "review/list"
        const val CALL_GET_SHELVES = "shelf/list.xml"
        const val CALL_GET_USER = "user/show/"
        const val CALL_GET_USER_ID = "api/auth_user"
        const val CALL_SEARCH = "search/index.xml"

        val TAG = GoodreadsAPI::class.java.simpleName
    }
}
