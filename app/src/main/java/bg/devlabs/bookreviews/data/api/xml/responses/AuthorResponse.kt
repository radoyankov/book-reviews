package bg.devlabs.bookreviews.data.api.xml.responses

import org.simpleframework.xml.Element
import org.simpleframework.xml.Root

import bg.devlabs.bookreviews.data.models.Author

@Root(name = "GoodreadsResponse", strict = false)
class AuthorResponse {

    @Element
    val author: Author? = null

}