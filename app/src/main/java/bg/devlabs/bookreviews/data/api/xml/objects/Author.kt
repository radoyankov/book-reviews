package bg.devlabs.bookreviews.data.api.xml.objects

import org.simpleframework.xml.Element
import org.simpleframework.xml.ElementList
import org.simpleframework.xml.Root

@Root
class Author {
    @Element
    private val id: String? = null

    /**
     * The Goodreads user id of the author, if there is one.
     *
     * @return
     */
    val userId: String?
        get() = user?.id

    @Element
    val name: String? = null

    @Element(data = true)
    private val link: String? = null

    @Element(name = "fans_count")
    private val fansCount: Int = 0

    @Element(name = "image_url", data = true)
    private val imageUrl: String? = null

    @Element(name = "small_image_url", data = true)
    private val smallImageUrl: String? = null

    @Element(required = false)
    private val about: String? = null

    @Element(required = false)
    private val influences: String? = null

    @Element(name = "works_count")
    private val worksCount: Int = 0

    @Element(required = false)
    private val gender: String? = null

    @Element(required = false)
    private val hometown: String? = null

    @Element(name = "born_at", required = false)
    private val bornAt: String? = null

    @Element(name = "died_at", required = false)
    private val diedAt: String? = null

    @Element(required = false)
    private val user: UserInternal? = null

    @ElementList(required = false)
    private val books: List<Book>? = null

    @Root
    private class UserInternal {
        @Element
        internal var id: String? = null
    }
}