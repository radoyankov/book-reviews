package bg.devlabs.bookreviews.data.local

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import bg.devlabs.bookreviews.data.models.LocalBook

/**
 * Created by Radoslav Yankov on 31.05.2018
 * Dev Labs
 * radoslavyankov@gmail.com
 */
@Database(entities = [LocalBook::class],
        version = 7, exportSchema = false)
abstract//@TypeConverters({Converters.class})
class AppDatabase : RoomDatabase() {
    abstract fun localBookDao(): LocalBookDao
}