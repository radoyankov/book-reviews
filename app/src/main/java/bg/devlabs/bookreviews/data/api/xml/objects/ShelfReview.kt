package bg.devlabs.bookreviews.data.api.xml.objects

import org.simpleframework.xml.Default
import org.simpleframework.xml.Element
import org.simpleframework.xml.Root

/**
 * Created by Radoslav Yankov on 04.05.2018
 * Dev Labs
 * radoslavyankov@gmail.com
 */
@Root(name = "review", strict = false)
data class ShelfReview @JvmOverloads constructor(
        @param:Element(name = "book", required = false)
        @field:Element(name = "book", required = false)
        val book: Book? = null
)