package bg.devlabs.bookreviews.data.api.auth

enum class OAuthLoginDialogType {
    FULLSCREEN, DIALOG
}
