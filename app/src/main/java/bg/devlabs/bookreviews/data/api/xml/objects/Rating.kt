package bg.devlabs.bookreviews.data.api.xml.objects

import org.simpleframework.xml.Root

/**
 * Created by Radoslav Yankov on 02.05.2018
 * Dev Labs
 * radoslavyankov@gmail.com
 */
@Root(name = "rating", strict = false)
class Rating(val rating: String): TypeOfAction