package bg.devlabs.bookreviews.data.prefs

import android.content.Context
import android.content.SharedPreferences
import bg.devlabs.bookreviews.data.models.FullBook
import bg.devlabs.bookreviews.di.ApplicationContext
import bg.devlabs.bookreviews.di.PreferenceInfo
import com.google.gson.Gson
import javax.inject.Inject

/**
 * Created by Radoslav Yankov on 26.10.2017
 * Dev Labs
 * radoslavyankov@gmail.com
 */

class AppPreferencesHelper @Inject
constructor(@ApplicationContext context: Context
            , @PreferenceInfo prefFileName: String
) : PreferencesHelper {
    override var onboardingPassed: Boolean
        get() = prefs.getBoolean(keyOnboarding, false)
        set(value) {
            prefs.edit().putBoolean(keyOnboarding, value).apply()
        }

    override var userId: String?
        get() = prefs.getString(keyUserId, "")
        set(value) {
            prefs.edit().putString(keyUserId, value).apply()
        }

    override var lastBook: FullBook?
        get() {
            return try {
                Gson().fromJson(prefs.getString(keyLastBook, ""), FullBook::class.java)
            } catch (e:Exception){
                null
            }
        }
        set(value) {
            prefs.edit().putString(keyLastBook, Gson().toJson(value)).apply()
        }

    private val prefs: SharedPreferences = context.getSharedPreferences(prefFileName, Context.MODE_PRIVATE)

    companion object {
        private val keyOnboarding = "keyOnboarding"
        private val keyUserId = "keyUserId"
        private val keyLastBook = "lastBook"
    }

}
