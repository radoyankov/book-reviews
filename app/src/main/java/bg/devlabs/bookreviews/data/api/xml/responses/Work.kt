package bg.devlabs.bookreviews.data.api.xml.responses

import bg.devlabs.bookreviews.data.models.Book
import org.simpleframework.xml.Element
import org.simpleframework.xml.Root

/**
 * Created by Radoslav Yankov on 28.05.2018
 * Dev Labs
 * radoslavyankov@gmail.com
 */
@Root(name = "work", strict = false)
data class Work @JvmOverloads constructor(
        @param:Element(name = "best_book", required = false)
        @field:Element(name = "best_book", required = false)
        val best_book: Book
)