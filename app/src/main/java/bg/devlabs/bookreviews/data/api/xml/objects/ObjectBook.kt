package bg.devlabs.bookreviews.data.api.xml.objects

import java.util.ArrayList
import java.util.Collections

import org.simpleframework.xml.Element
import org.simpleframework.xml.ElementList
import org.simpleframework.xml.Root

@Root(name = "book", strict = false)
data class ObjectBook @JvmOverloads constructor(
        @param:Element(name = "id", required = false)
        @field:Element(name = "id", required = false)
        val id: Int?,
        @param:Element(name = "title", required = false)
        @field:Element(name = "title", required = false)
        val title: String?,
        @param:Element(name = "link", required = false)
        @field:Element(name = "link", required = false)
        val link: String?,
        @param:ElementList(name = "authors", required = false)
        @field:ElementList(name = "authors", required = false)
        val authors: List<AuthorInternal>? = null
) : Obj


