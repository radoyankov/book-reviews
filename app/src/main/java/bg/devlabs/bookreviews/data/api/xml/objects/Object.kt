package bg.devlabs.bookreviews.data.api.xml.objects

import org.simpleframework.xml.ElementUnion
import org.simpleframework.xml.Root
import org.simpleframework.xml.Element

/**
 * Created by Radoslav Yankov on 02.05.2018
 * Dev Labs
 * radoslavyankov@gmail.com
 */
@Root(name = "object", strict = false)
data class Object @JvmOverloads constructor(
        @field:ElementUnion(Element(name = "read_status", type = ReadStatus::class), Element(name = "book", type = ObjectBook::class))
        @param:ElementUnion(Element(name = "read_status", type = ReadStatus::class), Element(name = "book", type = ObjectBook::class))
        val obj: Obj?
)