package bg.devlabs.bookreviews.data.models

import org.simpleframework.xml.Element
import org.simpleframework.xml.Root

/**
 * Created by Radoslav Yankov on 26.10.2017
 * Dev Labs
 * radoslavyankov@gmail.com
 */
@Root(name = "author", strict = false)
data class Author @JvmOverloads constructor(
        @field:Element(name = "name", required = false)
        @param:Element(name = "name", required = false)
        var name: String = "",
        @field:Element(name = "id", required = false)
        @param:Element(name = "id", required = false)
        var id: Int = 0
)