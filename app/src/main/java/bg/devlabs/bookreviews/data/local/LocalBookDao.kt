package bg.devlabs.bookreviews.data.local

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import bg.devlabs.bookreviews.data.models.LocalBook
import io.reactivex.Single

@Dao
abstract class LocalBookDao: BaseDao<LocalBook> {

    /**
     * Get the Admins from the table.
     *
     * @return all Admins from the table as [LiveData]
     */
    @Query("SELECT * FROM local_book")
    abstract fun getLocals(): Single<List<LocalBook>>

    @Query("SELECT * FROM local_book WHERE id = :id")
    abstract fun getLocalsById(id: String): Single<LocalBook>

    @Query("DELETE FROM local_book")
    abstract fun deleteAll()

    @Query("DELETE FROM local_book WHERE id = :id")
    abstract fun deleteById(id: String)

}
