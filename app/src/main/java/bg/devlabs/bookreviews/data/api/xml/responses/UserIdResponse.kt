package bg.devlabs.bookreviews.data.api.xml.responses

import bg.devlabs.bookreviews.data.api.xml.objects.UserId
import org.simpleframework.xml.Element
import org.simpleframework.xml.Root

/**
 * Created by Radoslav Yankov on 03.05.2018
 * Dev Labs
 * radoslavyankov@gmail.com
 */
@Root(name = "GoodreadsResponse", strict = false)
data class UserIdResponse @JvmOverloads constructor(
        @param:Element(name = "user", required = false)
        @field:Element(name = "user", required = false)
        val user: UserId
)