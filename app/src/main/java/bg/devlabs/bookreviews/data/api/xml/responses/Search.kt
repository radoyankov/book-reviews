package bg.devlabs.bookreviews.data.api.xml.responses

import org.simpleframework.xml.ElementList
import org.simpleframework.xml.Root

/**
 * Created by Radoslav Yankov on 28.05.2018
 * Dev Labs
 * radoslavyankov@gmail.com
 */

@Root(name = "search", strict = false)
data class Search @JvmOverloads constructor(
        @field:ElementList(name = "results", required = false)
        @param:ElementList(name = "results", required = false)
        var results: List<Work>?
)