package bg.devlabs.bookreviews.data.models

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

/**
 * Created by Radoslav Yankov on 31.05.2018
 * Dev Labs
 * radoslavyankov@gmail.com
 */
@Entity(tableName = "local_book")
data class LocalBook(
        @PrimaryKey @ColumnInfo(name = "id") var id: String, var title: String = "",
        var image: String = "", var description: String = "", var rating: String = "",
        var author: String = "", var ratings_widget: String = "", var link: String = "")