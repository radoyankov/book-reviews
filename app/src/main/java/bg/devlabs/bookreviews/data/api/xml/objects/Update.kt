package bg.devlabs.bookreviews.data.api.xml.objects

import org.simpleframework.xml.Attribute
import org.simpleframework.xml.Element
import org.simpleframework.xml.Root

/**
 * Created by Radoslav Yankov on 02.05.2018
 * Dev Labs
 * radoslavyankov@gmail.com
 */
@Root(name = "update", strict = false)
data class Update @JvmOverloads constructor(
        @field:Attribute(name = "type", required = false)
        @param:Attribute(name = "type", required = false)
        val type: String?,
        @field:Element(name = "action_text", required = false)
        @param:Element(name = "action_text", required = false)
        val action_text: String?,
        @field:Element(name = "link", required = false)
        @param:Element(name = "link", required = false)
        val link: String?,
        @field:Element(name = "image_url", required = false)
        @param:Element(name = "image_url", required = false)
        val image_url: String?,
        @field:Element(name = "actor", required = false)
        @param:Element(name = "actor", required = false)
        val actor: User?,
        @field:Element(name = "updated_at", required = false)
        @param:Element(name = "updated_at", required = false)
        val updated_at: String?,
        @field:Element(name = "action", required = false)
        @param:Element(name = "action", required = false)
        val action: Action?,
        @field:Element(name = "object", required = false)
        @param:Element(name = "object", required = false)
        val obj: Object? = null
)