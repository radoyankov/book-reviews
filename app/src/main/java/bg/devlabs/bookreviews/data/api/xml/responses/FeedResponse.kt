package bg.devlabs.bookreviews.data.api.xml.responses

import bg.devlabs.bookreviews.data.api.xml.objects.Update
import org.simpleframework.xml.Element
import org.simpleframework.xml.ElementList
import org.simpleframework.xml.Root

/**
 * Created by Radoslav Yankov on 02.05.2018
 * Dev Labs
 * radoslavyankov@gmail.com
 */
@Root(name = "GoodreadsResponse", strict = false)
data class FeedResponse @JvmOverloads constructor(
        @field:ElementList(name = "updates", required = false)
        @param:ElementList(name = "updates", required = false)
        var updates: List<Update>?
)