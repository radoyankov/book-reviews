package bg.devlabs.bookreviews.data.models

import java.io.Serializable

/**
 * Created by Radoslav Yankov on 26.10.2017
 * Dev Labs
 * radoslavyankov@gmail.com
 */
data class FullBook(var id: String, var title: String, var image: String, var description: String,
                    var rating: String, var author: String, var ratings_widget: String, var link: String) : Serializable
