package bg.devlabs.bookreviews.data.network

import bg.devlabs.bookreviews.BuildConfig
import bg.devlabs.bookreviews.data.models.GoodreadsResponse
import io.reactivex.Observable
import org.simpleframework.xml.core.Persister
import java.net.HttpURLConnection
import java.net.URL
import javax.inject.Inject

/**
 * Created by Radoslav Yankov on 26.10.2017
 * Dev Labs
 * radoslavyankov@gmail.com
 */

class AppApiHelper @Inject
constructor(
//        retrofit: Retrofit
) : ApiHelper {
    override fun getBookReviewsLink(isbn: String, page: Int): String {
        return "${BuildConfig.BASE_URL_REVIEW}$isbn&amp;page=$page"
    }

    override fun getBookInfo(isbn: String, page: Int): Observable<GoodreadsResponse> {
        return Observable.create { emitter ->
            val url = "${BuildConfig.BASE_URL}/book/isbn/$isbn?key=${BuildConfig.API_KEY}"
            val link: URL
            val conn: HttpURLConnection
            val serializer = Persister()
            var feed = GoodreadsResponse()
            link = URL(url)
            conn = link.openConnection() as HttpURLConnection
            try {
                conn.connect()
                feed = serializer.read<GoodreadsResponse>(GoodreadsResponse::class.java, conn.inputStream)
            } catch (e: Exception) {
                if (!emitter.isDisposed) {
                    emitter.onError(e)
                    conn.disconnect()
                }
            }
            if (!emitter.isDisposed) {
                emitter.onNext(feed)
                conn.disconnect()
            }
        }
    }

}
