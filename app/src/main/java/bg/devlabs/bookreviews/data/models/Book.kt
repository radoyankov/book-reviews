package bg.devlabs.bookreviews.data.models

import org.simpleframework.xml.Element
import org.simpleframework.xml.Root

/**
 * Created by Radoslav Yankov on 26.10.2017
 * Dev Labs
 * radoslavyankov@gmail.com
 */
@Root(strict = false)
data class Book(

        @field:Element(name = "id")
        var id: String = "",

        @field:Element(name = "title")
        var title: String = "",

        @field:Element(name = "image_url", required = false)
        var imageUrl: String = "",

        @field:Element(name = "description", required = false)
        var description: String = "",

        @field:Element(name = "average_rating", required = false)
        var averageRating: String = "",

        @field:Element(name = "authors", required = false)
        @param:Element(name = "authors", required = false)
        var authors: Authors? = null,


        @field:Element(name = "author", required = false)
        @param:Element(name = "author", required = false)
        var author: Author? = null,

        @field:Element(name = "reviews_widget", data = true, required = false)
        var reviews: String = "",

        @field:Element(name = "url", data = true, required = false)
        var link: String = ""
){
        constructor(): this("")
}