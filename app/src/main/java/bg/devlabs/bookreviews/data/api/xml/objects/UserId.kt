package bg.devlabs.bookreviews.data.api.xml.objects

import org.simpleframework.xml.Attribute
import org.simpleframework.xml.Root

/**
 * Created by Radoslav Yankov on 03.05.2018
 * Dev Labs
 * radoslavyankov@gmail.com
 */

@Root(name = "user", strict = false)
data class UserId @JvmOverloads constructor(
        @field:Attribute(name = "id")
        @param:Attribute(name = "id")
        var id: String)