package bg.devlabs.bookreviews.data.models

import org.simpleframework.xml.Element
import org.simpleframework.xml.Root

/**
 * Created by Radoslav Yankov on 26.10.2017
 * Dev Labs
 * radoslavyankov@gmail.com
 */
@Root(name = "GoodreadsResponse", strict = false)
data class GoodreadsResponse(
        @field:Element(name = "book")
        var book: Book? = null
)
//{
//        constructor(): this(Book())
//}