package bg.devlabs.bookreviews.data.local

import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy

/**
 * Created by Simona Stoyanova on 4/19/18.
 * simona@devlabs.bg
 *
 * Base dao class with insert and delete queries
 *
 * For example see:
 * https://gist.github.com/florina-muntenescu/1c78858f286d196d545c038a71a3e864
 */
interface BaseDao<T> {

    /**
     * Insert an object in the database.
     * If this item is already in the DB, replaces it
     *
     * @param obj the object to be inserted.
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(obj: T)

    /**
     * Delete an object from the database
     *
     * @param obj the object to be deleted
     */
    @Delete
    fun delete(obj: T): Int
}