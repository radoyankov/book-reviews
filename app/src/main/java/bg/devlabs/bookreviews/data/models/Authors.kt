package bg.devlabs.bookreviews.data.models

import org.simpleframework.xml.Element
import org.simpleframework.xml.Root



/**
 * Created by Radoslav Yankov on 26.10.2017
 * Dev Labs
 * radoslavyankov@gmail.comn
 */
@Root(name = "authors", strict = false)
data class Authors @JvmOverloads constructor(
        @field:Element(name = "author")
        @param:Element(name = "author")
        var author: Author? = null
)