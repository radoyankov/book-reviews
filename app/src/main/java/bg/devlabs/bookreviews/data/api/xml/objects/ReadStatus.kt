package bg.devlabs.bookreviews.data.api.xml.objects

import bg.devlabs.bookreviews.R.id.name
import org.simpleframework.xml.Element
import org.simpleframework.xml.Root

/**
 * Created by Radoslav Yankov on 02.05.2018
 * Dev Labs
 * radoslavyankov@gmail.com
 */
@Root(name = "read_status", strict = false)
data class ReadStatus @JvmOverloads constructor(
        @field:Element(name = "id")
        @param:Element(name = "id")
        val id: String?,
        @field:Element(name = "review_id", required = false)
        @param:Element(name = "review_id", required = false)
        val review_id: String?,
        @field:Element(name = "user_id", required = false)
        @param:Element(name = "user_id", required = false)
        val user_id: String?,
        @field:Element(name = "old_status", required = false)
        @param:Element(name = "old_status", required = false)
        val old_status: String?,
        @field:Element(name = "status", required = false)
        @param:Element(name = "status", required = false)
        val status: String?,
        @field:Element(name = "review", required = false)
        @param:Element(name = "review", required = false)
        val review: Review?
        ) : Obj