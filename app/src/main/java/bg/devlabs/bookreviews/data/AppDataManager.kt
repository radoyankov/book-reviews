package bg.devlabs.bookreviews.data

import android.content.Context
import bg.devlabs.bookreviews.data.models.FullBook
import bg.devlabs.bookreviews.data.models.GoodreadsResponse
import bg.devlabs.bookreviews.data.network.ApiHelper
import bg.devlabs.bookreviews.data.prefs.PreferencesHelper
import bg.devlabs.bookreviews.di.ApplicationContext
import bg.devlabs.bookreviews.utils.*
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
 * Created by Radoslav Yankov on 26.10.2017
 * Dev Labs
 * radoslavyankov@gmail.com
 */

class AppDataManager @Inject
constructor(@param:ApplicationContext val mContext: Context,
            val preferencesHelper: PreferencesHelper,
            val apiHelper: ApiHelper) : DataManager {
    override var onboardingPassed: Boolean
        get() = preferencesHelper.onboardingPassed
        set(value) {
            preferencesHelper.onboardingPassed = value
        }
    override var lastBook: FullBook?
        get() = preferencesHelper.lastBook
        set(value) {
            preferencesHelper.lastBook = value
        }

    override var userId: String?
        get() = preferencesHelper.userId
        set(value) {
            preferencesHelper.userId = value
        }

    override fun getBookInfo(isbn: String, page: Int): Observable<GoodreadsResponse> {
        return apiHelper.getBookInfo(isbn, page).prepare()
    }

    override fun getBook(isbn: String, page: Int): Observable<FullBook> {

        return getBookInfo(isbn, page).map { response ->
            response.book.let {
                FullBook(it?.id.orEmpty(), it?.title.orEmpty(), it?.imageUrl.orEmpty(),
                        it?.description.orEmpty(), it?.averageRating.orEmpty(),
                        it?.authors?.author?.name ?: defaultAuthor, it?.reviews.orEmpty(), it?.link.orEmpty())
            }
        }
    }

    override fun getBookReviewsLink(isbn: String, page: Int): String {
        return apiHelper.getBookReviewsLink(isbn, page)
    }

    fun <T> Observable<T>.prepare() = this.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())

    fun <T> Observable<T>.threads(subOn: Scheduler, obsOn: Scheduler) = this.subscribeOn(subOn).observeOn(obsOn)

}
