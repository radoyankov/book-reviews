package bg.devlabs.bookreviews.data.network

import bg.devlabs.bookreviews.data.models.GoodreadsResponse
import io.reactivex.Observable


/**
 * Created by Radoslav Yankov on 26.10.2017
 * Dev Labs
 * radoslavyankov@gmail.com
 */

interface ApiHelper {
    /**
     * Gets info about a book. Requires isbn and a page number for the page of reviews
     */
    fun getBookInfo(isbn: String, page: Int): Observable<GoodreadsResponse>

    /**
     * Gets the link for the book reviews
     */
    fun getBookReviewsLink(isbn: String, page: Int) : String

}