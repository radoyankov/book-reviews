package bg.devlabs.bookreviews.data.api.xml.objects

import org.simpleframework.xml.Element
import org.simpleframework.xml.ElementList
import org.simpleframework.xml.Root

@Root(name = "user", strict = false)
data class User @JvmOverloads constructor(
        @field:Element(name = "id")
        @param:Element(name = "id")
        var id: Int,
        @field:Element(name = "name", required = false)
        @param:Element(name = "name", required = false)
        var name: String,
        @field:Element(name = "user_name", required = false)
        @param:Element(name = "user_name", required = false)
        var user_name: String?,
        @field:Element(name = "link", data = true, required = false)
        @param:Element(name = "link", data = true, required = false)
        var link: String?,
        @field:Element(name = "image_url", data = true, required = false)
        @param:Element(name = "image_url", data = true, required = false)
        var image_url: String?,
        @field:Element(name = "small_image_url", data = true, required = false)
        @param:Element(name = "small_image_url", data = true, required = false)
        var small_image_url: String?,
        @field:Element(name = "about", required = false)
        @param:Element(name = "about", required = false)
        var about: String?,
        @field:Element(name = "age", required = false)
        @param:Element(name = "age", required = false)
        var age: Int?,
        @field:Element(name = "gender", required = false)
        @param:Element(name = "gender", required = false)
        var gender: String?,
        @field:Element(name = "location", required = false)
        @param:Element(name = "location", required = false)
        var location: String?,
        @field:Element(name = "website", required = false)
        @param:Element(name = "website", required = false)
        var website: String?,
        @field:Element(name = "joined", required = false)
        @param:Element(name = "joined", required = false)
        var joined: String?,
        @field:Element(name = "last_active", required = false)
        @param:Element(name = "last_active", required = false)
        var last_active: String?,
        @field:Element(name = "interests", required = false)
        @param:Element(name = "interests", required = false)
        var interests: String?,
        @field:Element(name = "favorite_books", required = false)
        @param:Element(name = "favorite_books", required = false)
        var favorite_books: String?,
        @field:Element(name = "favorite_authors", required = false)
        @param:Element(name = "favorite_authors", required = false)
        var favorite_authors: String?,
        @field:Element(name = "friend", required = false)
        @param:Element(name = "friend", required = false)
        var friend: Boolean?,
        @field:Element(name = "friend_status", required = false)
        @param:Element(name = "friend_status", required = false)
        var friend_status: String?,
        @field:Element(name = "updates_rss_url", data = true, required = false)
        @param:Element(name = "updates_rss_url", data = true, required = false)
        var updates_rss_url: String?,
        @field:Element(name = "reviews_rss_url", data = true, required = false)
        @param:Element(name = "reviews_rss_url", data = true, required = false)
        var reviews_rss_url: String?,
        @field:Element(name = "friends_count", required = false)
        @param:Element(name = "friends_count", required = false)
        var friends_count: Int?,
        @field:Element(name = "groups_count", required = false)
        @param:Element(name = "groups_count", required = false)
        var groups_count: Int?,
        @field:Element(name = "reviews_count", required = false)
        @param:Element(name = "reviews_count", required = false)
        var reviews_count: Int?
        ,
        @field:ElementList(name = "user_shelves", required = false)
        @param:ElementList(name = "user_shelves", required = false)
        var user_shelves: List<UserShelf>?
//        var user_shelves: ArrayList<UserShelf>,
//        @ElementList

)
