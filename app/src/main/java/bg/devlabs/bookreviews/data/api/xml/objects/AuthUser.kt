package bg.devlabs.bookreviews.data.api.xml.objects

import org.simpleframework.xml.Attribute
import org.simpleframework.xml.Element
import org.simpleframework.xml.Root

@Root
class AuthUser {

    @Attribute
    val id: String? = null

    @Element
    val name: String? = null

    @Element
    val link: String? = null
}