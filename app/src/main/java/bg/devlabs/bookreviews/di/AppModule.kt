package bg.devlabs.bookreviews.di

import android.app.Application
import android.arch.persistence.room.Room
import android.content.Context
import bg.devlabs.bookreviews.data.AppDataManager
import bg.devlabs.bookreviews.data.DataManager
import bg.devlabs.bookreviews.data.local.AppDatabase
import bg.devlabs.bookreviews.data.local.LocalBookDao
import bg.devlabs.bookreviews.data.network.ApiHelper
import bg.devlabs.bookreviews.data.network.AppApiHelper
import bg.devlabs.bookreviews.data.prefs.AppPreferencesHelper
import bg.devlabs.bookreviews.data.prefs.PreferencesHelper

import javax.inject.Singleton

import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable

/**
 * Created by Radoslav Yankov on 26.10.2017
 * Dev Labs
 * radoslavyankov@gmail.com
 */

@Module
internal class AppModule{

    @Provides
    @ApplicationContext
    internal fun provideContext(application: Application): Context {
        return application
    }

    @Provides
    @PreferenceInfo
    internal fun providePreferenceName(): String {
        return "book_revi"
    }

    @Provides
    @Singleton
    internal fun provideDataManager(appDataManager: AppDataManager): DataManager {
        return appDataManager
    }

    @Singleton
    @Provides
    fun provideDb(context: Context): AppDatabase {
        return Room.databaseBuilder(context, AppDatabase::class.java, "books.db")
                .fallbackToDestructiveMigration()
                .build()
    }

    @Singleton
    @Provides
    fun provideLocalBooksDao(db: AppDatabase): LocalBookDao {
        return db.localBookDao()
    }

    @Provides
    @Singleton
    internal fun provideApiHelper(appApiHelper: AppApiHelper): ApiHelper {
        return appApiHelper
    }

    @Provides
    @Singleton
    internal fun providePreferencesHelper(appPreferencesHelper: AppPreferencesHelper): PreferencesHelper {
        return appPreferencesHelper
    }

    @Provides
    internal fun provideCompositeDisposable(): CompositeDisposable {
        return CompositeDisposable()
    }
}