package bg.devlabs.bookreviews.di

import bg.devlabs.bookreviews.ui.bookshelves.BookshelvesContract
import bg.devlabs.bookreviews.ui.bookshelves.BookshelvesFragment
import dagger.Module
import dagger.Provides

@Module
class BookshelvesFragmentModule {
    @Provides
    fun provideBookshelfView(fragment: BookshelvesFragment): BookshelvesContract.View{
        return fragment
    }
}
