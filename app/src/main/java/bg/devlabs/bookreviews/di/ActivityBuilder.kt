package bg.devlabs.bookreviews.di

import bg.devlabs.bookreviews.ui.bookshelves.BookshelvesFragment
import bg.devlabs.bookreviews.ui.bookshelves.FeedFragment
import bg.devlabs.bookreviews.ui.bookshelves.ProfileFragment
import bg.devlabs.bookreviews.ui.home.HomeActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by Radoslav Yankov on 26.10.2017
 * Dev Labs
 * radoslavyankov@gmail.com
 */
@Module
abstract class ActivityBuilder {
    @ContributesAndroidInjector(modules = arrayOf(HomeActivityModule::class))
    abstract fun contributeMainActivity(): HomeActivity
    @ContributesAndroidInjector(modules = arrayOf(BookshelvesFragmentModule::class))
    abstract fun contributeBookshelfFragment(): BookshelvesFragment
    @ContributesAndroidInjector(modules = arrayOf(FeedFragmentModule::class))
    abstract fun contributeFeedFragment(): FeedFragment
    @ContributesAndroidInjector(modules = arrayOf(ProfileFragmentModule::class))
    abstract fun contributeProfileFragment(): ProfileFragment
}
