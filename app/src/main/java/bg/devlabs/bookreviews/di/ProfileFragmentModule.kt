package bg.devlabs.bookreviews.di

import bg.devlabs.bookreviews.ui.bookshelves.FeedContract
import bg.devlabs.bookreviews.ui.bookshelves.FeedFragment
import bg.devlabs.bookreviews.ui.bookshelves.ProfileContract
import bg.devlabs.bookreviews.ui.bookshelves.ProfileFragment
import dagger.Module
import dagger.Provides

@Module
class ProfileFragmentModule {
    @Provides
    fun provideProfileView(fragment: ProfileFragment): ProfileContract.View{
        return fragment
    }
}
