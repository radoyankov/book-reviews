package bg.devlabs.bookreviews.di

import javax.inject.Qualifier

/**
 * Created by Radoslav Yankov on 26.10.2017
 * Dev Labs
 * radoslavyankov@gmail.com
 */
@Qualifier
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
annotation class PreferenceInfo

annotation class ApplicationContext
