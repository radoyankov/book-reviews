package bg.devlabs.bookreviews.di

import bg.devlabs.bookreviews.ui.home.HomeActivity
import bg.devlabs.bookreviews.ui.home.HomeContract
import dagger.Module
import dagger.Provides

/**
 * Created by Radoslav Yankov on 26.10.2017
 * Dev Labs
 * radoslavyankov@gmail.com
 */
@Module
class HomeActivityModule {
    @Provides
    fun provideHomeView(activity: HomeActivity): HomeContract.View {
        return activity
    }
}
