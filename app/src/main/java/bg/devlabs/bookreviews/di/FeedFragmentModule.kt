package bg.devlabs.bookreviews.di

import bg.devlabs.bookreviews.ui.bookshelves.FeedContract
import bg.devlabs.bookreviews.ui.bookshelves.FeedFragment
import dagger.Module
import dagger.Provides

@Module
class FeedFragmentModule {
    @Provides
    fun provideFeedView(fragment: FeedFragment): FeedContract.View{
        return fragment
    }
}
